#!/usr/bin/perl

use strict;
use warnings;
use Data::Dumper;
use POSIX;

open (VCF, "<$ARGV[0]");

my $wanted_SNPs = 100;


my %hash;
my $scaffold;

while (<VCF>) {
    
    
    chomp;
    if ($_ =~ m/^#/) {
        next;
    }
    
    $scaffold = (split("\t",$_))[0];
    
    
    $hash{$scaffold}{'count'}++;

}
close(VCF);

foreach my $scaffold (keys(%hash)) {
    if ($hash{$scaffold}{'count'} > $wanted_SNPs) {
        my $float_parts = $hash{$scaffold}{'count'} / $wanted_SNPs;
        
        my $interger_div = floor($float_parts);
        
        my $split_point= ceil($hash{$scaffold}{'count'} / $interger_div);
        
        $hash{$scaffold}{'split_point'} = $split_point;
    }
}



#print Dumper \%hash;

open (VCF, "<$ARGV[0]");

my $block_num = 1;
my $counter = 1;
my $prev_scaff = 'none';

while (<VCF>) {
	chomp;
	if ($_ !~ m/^#/) {
		
		my @line = split ("\t", $_);
		my $scaff = $line[0];
		
		if ($scaff ne $prev_scaff) {
			$counter = 1;
			$block_num = 1;
		}
		
		if ($hash{$scaff}{'split_point'}) {
			
			if ($counter eq ($hash{$scaff}{'split_point'}+1)) {
				$block_num++;
				$counter = 1;
			}
			
			$line[0] = $scaff."_BLOCK_".$block_num;
			
			my $line = join ("\t", @line);
			print $line."\n";
		
		}
		
		else {
			print $_."\n";
		}
		
		$prev_scaff = $scaff;
		$counter++;
		
	
	}
	
	else {
		print $_."\n";
	}

	
	
}




