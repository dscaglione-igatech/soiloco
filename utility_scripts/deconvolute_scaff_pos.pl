use strict;
use warnings;
use Data::Dumper;
use Statistics::TTest;



open (CSV1, "<$ARGV[0]");
open (CSV2, "<$ARGV[1]");
my %prehash1;
my %prehash1_ranked;
my %prehash2;
my %prehash2_ranked;
my $nind1;
my $nind2;
my @ind1;
my @ind2;

my %all_scaffolds;

####################### stage in data for map1
while (<CSV1>) {
	chomp;
	if ($_ =~ m/^id/) {
		my @line = split (",", $_);
		@ind1=@line[3..@line-1];
		$nind1= @ind1;
		next;
	}
	
	
	my @line = split (",", $_);
	
	my @suffixes = split ("_", $line[0]);
	my $scaffold = $suffixes[1];
	$all_scaffolds{$scaffold}++;
	
	my $block = $suffixes[3];
	@{$prehash1{$line[1]}{$scaffold}{$block}} = ($line[2], @line[3..@line-1]); 
    
}
close (CSV1);

#print Dumper \%all_scaffolds1;

####################### stage in data for map2
while (<CSV2>) {
	chomp;
	if ($_ =~ m/^id/) {
		my @line = split (",", $_);
		@ind2=@line[3..@line-1];
		$nind2= @ind2;
		next;
	}
	
	
	my @line = split (",", $_);
	
	my @suffixes = split ("_", $line[0]);
	my $scaffold = $suffixes[1];
	$all_scaffolds{$scaffold}++; # collect also list of scaffolds, with counts 
	
	my $block = $suffixes[3]; # get block pos
	@{$prehash2{$line[1]}{$scaffold}{$block}} = ($line[2], @line[3..@line-1]); # populate a big hash with all the information

}

close (CSV2);

######### suffix to individual names ############

@ind1 = map {$_.="_map1"} @ind1;
@ind2 = map {$_.="_map2"} @ind2;

#### look up for best links ########################################################

my %links_temp;
my %linked;
foreach my $chrA (keys %prehash1) {
	foreach my $chrB (keys %prehash2){
		foreach my $scaffold (keys %{$prehash1{$chrA}}){
			if (exists $prehash2{$chrB}{$scaffold}) {
				$links_temp{$chrA}{$chrB}++;
			}
		}
	}
	my $linked =(sort {$links_temp{$chrA}{$b} <=> $links_temp{$chrA}{$a}} keys %{$links_temp{$chrA}})[0];
	
    $linked{$chrA}=$linked; #get a hash representing the strongest relationship for each lg of set1 with lg of set 2, assume no ambiguity +> value is the lg of map2
    print "$chrA\t$linked\n";
}

print Dumper \%links_temp;
####################################################################################


### skim spurious placements########################################################


my %pos_counts1;
my %clean_pos;

foreach my $scaffold (keys %all_scaffolds) {
	foreach my $chrA (keys %prehash1) {  # arbitrarily use one of the two hashes to loop on chrs
		foreach my $block (keys %{$prehash1{$chrA}{$scaffold}}){ # count from set1
			$pos_counts1{$scaffold}{$chrA}++;
		}
		foreach my $block (keys %{$prehash2{$linked{$chrA}}{$scaffold}}){ # and set 2 using link information
			$pos_counts1{$scaffold}{$chrA}++;
		}
	}
	my $clean_position = (sort {$pos_counts1{$scaffold}{$b} <=> $pos_counts1{$scaffold}{$a}} keys %{$pos_counts1{$scaffold}})[0];
	#print "this scaffold $scaffold is definetively in $clean_position by $pos_counts1{$scaffold}{$clean_position} occurrences\n";
	$clean_pos{$scaffold} = $clean_position; ##always refers to the chr nomencature of set1
	
}



########## so far just needed for getting clean positions (scaffold <=> lg relationship and map1 <=> map2 lgs relationships)#####

open (CSV1, "<$ARGV[0]");

my %hash;



while (<CSV1>){
	if ($_ =~ m/^id,/) {next;}
	
	my @line = split (",", $_);
	my @marker = split ("_", $line[0]);
	
    #print $clean_pos{$marker[1]}."\t".$line[2]."\n";
    
    
    
    if ($clean_pos{$marker[1]} == $line[1]) {   # here I implemented to use clean position from previous routine to check if the lg assignment is not spurious
        
        if (!exists $hash{$line[1]}{$marker[1]}) {
            $hash{$line[1]}{$marker[1]} = [[],[]];   # this way an empty array of array is inititated when the scaffold record is new in the loop
        }
    
    
        # work for map1
        # hash{chr}{scaffold} -> [block_start, map pos]
        push (@{$hash{$line[1]}{$marker[1]}->[0]}, $marker[3]);  
        push (@{$hash{$line[1]}{$marker[1]}->[1]}, $line[2]);
	}

}
#

#print Dumper \%hash;

my %dump_map1;
open (ORIENT, ">map1_orientation.txt");

foreach my $chr (keys %hash) {

	
	
	
	foreach my $scaffold (keys %{$hash{$chr}}) {
		# flip the array for functions below
		my $ref = $hash{$chr}{$scaffold};
		my $x = [];
		my @array_pos = @{$ref->[0]};
		my @array_cM  = @{$ref->[1]};
		
		my @array_cM_sort = sort { $a <=> $b } @array_cM;
		my $max_cM = $array_cM_sort[-1];
		my $min_cM = $array_cM_sort[0];
		
		
		for (my $i=0; $i<@array_pos; $i++) {
			$x->[$i+1][1] = $array_pos[$i];
			$x->[$i+1][2] = $array_cM[$i];
		
		}
		
		my ($pos_mean, $cM_mean) = mean($x); 
		#print "scaffold_$scaffold\t";
		#print $min_cM."\t".$cM_mean."\t".$max_cM."\t";#
		#print scalar @array_pos."\n";
		
		if (($max_cM != $min_cM) && (@array_pos > 2)) {
			my $tprob = "UNDEFINED";
			my $corr = correlation($x);
			my $t = abs($corr) * sqrt(@array_pos - 2) / sqrt (1 - $corr**2);
			$tprob = Statistics::Distributions::tprob (@array_pos -2 ,$t);
            
           
            
            if ($corr>=0.50) {
                printf ORIENT "LG${chr}\tscaffold_$scaffold\t%.2f\t+\t%.2f\t%.2f\n", $cM_mean, $corr, $tprob;
            }#
            elsif ($corr<=-0.50){
                printf ORIENT "LG${chr}\tscaffold_$scaffold\t%.2f\t-\t%.2f\t%.2f\n", $cM_mean, $corr, $tprob;
            }
            else {
                printf ORIENT "LG${chr}\tscaffold_$scaffold\t%.2f\t\.\t%.2f\t%.2f\n", $cM_mean, $corr, $tprob;
            }
        
        
        }
        
		else {
			printf ORIENT "LG${chr}\tscaffold_$scaffold\t%.2f\t\.\tNA\tNA\n", $cM_mean;
		}
        # an ordered dump hash for mergemap
        $dump_map1{$chr}{$scaffold} = $cM_mean;
        
	}

	

}

close (ORIENT);


foreach my $chr (keys %dump_map1) {
    open (LG, ">map1_ordered_lg${chr}_avg.txt");
    print LG "group lg$chr\n;BEGINOFGROUP\n";
    
    foreach my $scaffold (sort {$dump_map1{$chr}{$a} <=> $dump_map1{$chr}{$b}} (keys %{$dump_map1{$chr}})) {
        printf LG "s%s\t%.2f\n", $scaffold, $dump_map1{$chr}{$scaffold};
    }
    
    print LG ";ENDOFGROUP\n\n";
    close (LG);

}




############################### now again for MAP 2, just consider links to map1 reference position ########################

open (CSV2, "<$ARGV[1]");
#print "MAP 2!!\n";
%hash=();
my %rev_linked = reverse %linked; ## this also assume no ambiguity

while (<CSV2>){
	if ($_ =~ m/^id,/) {next;}
	
	##AHAHA %rev_linked = reverse %linked; to get correspondance on map1 lg
	
    #$marker[1] is the scaffold
    #$line[1] is the lg for map2 here
	my @line = split (",", $_);
	my @marker = split ("_", $line[0]);
	
    #print $clean_pos{$marker[1]}."\t".$line[2]."\n";
    
    if ($clean_pos{$marker[1]} == $rev_linked{$line[1]} ) {   # here I implemented to use clean position from previous routine to check if the lg assignment is not spurious, but need to refere to map1 position
        
        if (!exists $hash{$rev_linked{$line[1]}}{$marker[1]}) {
            $hash{$rev_linked{$line[1]}}{$marker[1]} = [[],[]];   # this way an empty array of array is inititated when the scaffold record is new in the loop
        }
    
    
        # work for map1
        # hash{chr}{scaffold} -> [block_start, map pos]
        push (@{$hash{$rev_linked{$line[1]}}{$marker[1]}->[0]}, $marker[3]);  
        push (@{$hash{$rev_linked{$line[1]}}{$marker[1]}->[1]}, $line[2]);
	}

}
#

#print Dumper \%hash;

my %dump_map2;
open (ORIENT, ">map2_orientation.txt");

foreach my $chr (keys %hash) {

	
	
	
	foreach my $scaffold (keys %{$hash{$chr}}) {
		# flip the array for functions below
		my $ref = $hash{$chr}{$scaffold};
		my $x = [];
		my @array_pos = @{$ref->[0]};
		my @array_cM  = @{$ref->[1]};
		
		my @array_cM_sort = sort { $a <=> $b } @array_cM;
		my $max_cM = $array_cM_sort[-1];
		my $min_cM = $array_cM_sort[0];
		
		
		for (my $i=0; $i<@array_pos; $i++) {
			$x->[$i+1][1] = $array_pos[$i];
			$x->[$i+1][2] = $array_cM[$i];
		
		}
		
		my ($pos_mean, $cM_mean) = mean($x); 
		#print "scaffold_$scaffold\t";
		#print $min_cM."\t".$cM_mean."\t".$max_cM."\t";#
		#print scalar @array_pos."\n";
		
		if (($max_cM != $min_cM) && (@array_pos > 2)) {
			my $tprob = "UNDEFINED";
			my $corr = correlation($x);
			my $t = abs($corr) * sqrt(@array_pos - 2) / sqrt (1 - $corr**2);
			$tprob = Statistics::Distributions::tprob (@array_pos -2 ,$t);
            
           
            
            if ($corr>=0.50) {
                printf ORIENT "LG${chr}\tscaffold_$scaffold\t%.2f\t+\t%.2f\t%.2f\n", $cM_mean, $corr, $tprob;
            }#
            elsif ($corr<=-0.50){
                printf ORIENT "LG${chr}\tscaffold_$scaffold\t%.2f\t-\t%.2f\t%.2f\n", $cM_mean, $corr, $tprob;
            }
            else {
                printf ORIENT "LG${chr}\tscaffold_$scaffold\t%.2f\t\.\t%.2f\t%.2f\n", $cM_mean, $corr, $tprob;
            }
        
        
        }
        
		else {
			printf ORIENT "LG${chr}\tscaffold_$scaffold\t%.2f\t\.\tNA\tNA\n", $cM_mean;
		}
        # an ordered dump hash for mergemap
        $dump_map2{$chr}{$scaffold} = $cM_mean;
        
	}

	

}

close (ORIENT);


foreach my $chr (keys %dump_map2) {
    open (LG, ">map2_ordered_lg${chr}_avg.txt");
    print LG "group lg$chr\n;BEGINOFGROUP\n";
    
    foreach my $scaffold (sort {$dump_map2{$chr}{$a} <=> $dump_map2{$chr}{$b}} (keys %{$dump_map2{$chr}})) {
        printf LG "s%s\t%.2f\n", $scaffold, $dump_map2{$chr}{$scaffold};
    }
    
    print LG ";ENDOFGROUP\n\n";
    close (LG);
}


######################################################  SUBS #################################################

sub mean {
   my ($x)=@_;
   my $num = scalar(@{$x}) - 1;
   my $sum_x = '0';
   my $sum_y = '0';
   for (my $i = 1; $i < scalar(@{$x}); ++$i){
      $sum_x += $x->[$i][1];
      $sum_y += $x->[$i][2];
   }
   my $mu_x = $sum_x / $num;
   my $mu_y = $sum_y / $num;
   return($mu_x,$mu_y);
}
 
### ss = sum of squared deviations to the mean
sub ss {
   my ($x,$mean_x,$mean_y,$one,$two)=@_;
   my $sum = '0';
   for (my $i=1;$i<scalar(@{$x});++$i){
     $sum += ($x->[$i][$one]-$mean_x)*($x->[$i][$two]-$mean_y);
   }
   return $sum;
}
 
sub correlation {
   my ($x) = @_;
   my ($mean_x,$mean_y) = mean($x);
   my $ssxx=ss($x,$mean_x,$mean_y,1,1);
   my $ssyy=ss($x,$mean_x,$mean_y,2,2);
   my $ssxy=ss($x,$mean_x,$mean_y,1,2);
   my $correl=correl($ssxx,$ssyy,$ssxy);
   my $xcorrel=sprintf("%.4f",$correl);
   return($xcorrel);
 
}
 
sub correl {
   my($ssxx,$ssyy,$ssxy)=@_;
   my $sign=$ssxy/abs($ssxy);
   my $correl=$sign*sqrt($ssxy*$ssxy/($ssxx*$ssyy));
   return $correl;
}