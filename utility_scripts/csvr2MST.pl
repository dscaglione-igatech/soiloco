use strict;
use warnings;
use Data::Dumper;



my $pop_type = "DH";
my $pop_name = "$ARGV[0]";
my $function = "kosambi";
my $p_value = "2.0";
my $no_map_dist = "15.0";
my $no_map_size = "0";
my $max_missing = "1.00";
my $estimation = "no";
my $bad_data = "yes";
my $obj_fx = "ML";
 


my %conv = ("AA" => "A", 
         "AB" => "B",
         "-"  => "U",
         "BB" => "U");



#
open (CSVR, "<$ARGV[0]");

my $line = <CSVR>;
chomp $line;
my @individuals = split (",", $line);


@individuals = @individuals[3..@individuals-1];

#map { $_ =~ s/(.*)\.sorted\.rmdup\.bam/$1/} @individuals;
my $individuals = @individuals;
#print Dumper \@individuals;
my $loci = 0;
my %hash;
while (<CSVR>) {
	chomp;
    my @line = split (",", $_);
    #map ()
	my $marker_name = $line[0];
    @{$hash{$marker_name}} = @line[3..@line-1];
    $loci++;
}

# foreach my $marker (keys %hash){
    # map {}
    
# }

print "population_type $pop_type
population_name $pop_name
distance_function $function
cut_off_p_value $p_value
no_map_dist $no_map_dist
no_map_size $no_map_size
missing_threshold $max_missing
estimation_before_clustering $estimation
detect_bad_data $bad_data
objective_function $obj_fx
number_of_loci $loci
number_of_individual $individuals

";

print "locus_name\t";
my $ind_line = join("\t", @individuals);
print $ind_line."\n";

foreach my $marker (keys %hash){
    
    @{$hash{$marker}} = map {$conv{$_}} @{$hash{$marker}};
    my $string = join ("\t", @{$hash{$marker}});
    #print $string."\n";
    printf "%s\t%s\n", $marker, $string; 

}