#!/usr/bin/perl


#perl generate_two_circos.pl --map1=Endive.run2.sorted.csv --map2=Intibus.run2.sorted.csv --bins1=Endive.superbins --bins2=Intibus.superbins --mummer=promer.max.sim95 --name1=C.endivia --name2=C.intybus --prefix=scaffold --min_cont_hits=10

use strict;
use Getopt::Long;
use warnings;
use Data::Dumper;


my $offset_trick=100;
my $map1;
my $map2;
my $bins1;
my $bins2;
#my $stats1;
#my $stats2;
my $mummer_out;
my $name1;
my $name2;
#my $min_links = 5;
my $prefix1;
my $prefix2;
my $min_contiguous_hits=5;


GetOptions('map1=s' => \$map1, 'map2=s' => \$map2, 'bins1=s' => \$bins1, 'bins2=s' => \$bins2, 'mummer=s' => \$mummer_out, 'name1=s' => \$name1, 'name2=s' => \$name2, 'prefix1=s' => \$prefix1, 'prefix2=s' => \$prefix2,'min_cont_hits=i' => \$min_contiguous_hits);

my %map1;
my %map2;
my %bins1;
my %bins2;
my %stats1;
my %stats2;

open (MAP1, "<$map1");
print STDERR "Reading map1\n";
while (<MAP1>) {
	chomp;
	if ($_ =~ /^id/) {next;}
	my @line = split (",", $_);
	$map1{$line[1]}{$line[0]} = $line[2];  # record as HoH               {LG}-->{scaffold} = position
}
close (MAP1);

open (MAP2, "<$map2");
print STDERR "Reading map2\n";
while (<MAP2>) {
	chomp;
	if ($_ =~ /^id/) {next;}
	my @line = split (",", $_);
	$map2{$line[1]}{$line[0]} = $line[2];
}
close (MAP2);

open (BINS1, "<$bins1");
print STDERR "Reading bins1\n";
while (<BINS1>) {
	chomp;
	my @line = split ("\t",$_);
	shift @line;
	foreach my $element (@line) { 
        $bins1{$element} = $line[0];
    }
}
close (BINS1);

#print Dumper(\%bins1);

open (BINS2, "<$bins2");
print STDERR "Reading bins2\n";
while (<BINS2>) {
	chomp;
	my @line = split ("\t",$_);
	shift @line;
	foreach my $element (@line) { 
        $bins2{$element} = $line[0];
    }
}
close (BINS2);

# open (STATS1, "<$stats1");
# print STDERR "Reading stats1\n";
# while (<STATS1>) {
	# chomp;
	# my @line = split ("\t",$_);
	# $stats1{$line[0]} = $line[1];   # {scaffold} = size
# }
# close (STATS1);


# open (STATS2, "<$stats2");
# print STDERR "Reading stats2\n";
# while (<STATS2>) {
	# chomp;
	# my @line = split ("\t",$_);
	# $stats2{$line[0]} = $line[1];
# }
# close (STATS2);

#==================================================================================================
open (KARYO, ">karyotype.txt");
print STDERR "Printing Karyotype file\n";
foreach my $LG (sort {$a<=>$b} keys %map1) {
#print "$LG\n";
	my @pos_ord = sort {$a<=>$b} values %{$map1{$LG}};
	#print Dumper (\@pos_ord);
	my $size = 1000*($pos_ord[@pos_ord-1])+$offset_trick;   
	print KARYO "chr\t-\t"."$name1\_LG$LG\t"."$name1\_LG$LG\t"."0\t".$size."\t"."chr$LG\n";
}

foreach my $LG (sort {$a<=>$b} keys %map2) {
#print "$LG\n";
	my @pos_ord = sort {$a<=>$b} values %{$map2{$LG}};
	#print Dumper (\@pos_ord);
	my $size = 1000* ($pos_ord[@pos_ord-1])+$offset_trick;
	print KARYO "chr\t-\t"."$name2\_LG$LG\t"."$name2\_LG$LG\t"."0\t".$size."\t"."chr$LG\n";

	}
	
	##
	
foreach my $LG (sort {$a<=>$b} keys %map1) {
	#my $previous = 0;
	foreach my $scaffold (sort {$map1{$LG}{$a} <=> $map1{$LG}{$b}} keys %{$map1{$LG}}) {
		#if (undef $previous) {$previous = 0;}
		my $position1 = $map1{$LG}{$scaffold}*1000;
		my $position2 = $position1+$offset_trick;
		print KARYO "band\t$name1\_LG$LG\t$scaffold\t$scaffold\t$position1\t$position2\tgpos75\n";
		#$previous = $map1{$LG}{$scaffold};
	
	}


}

foreach my $LG (sort {$a<=>$b} keys %map2) {
	#my $previous = 0;
	foreach my $scaffold (sort {$map2{$LG}{$a} <=> $map2{$LG}{$b}} keys %{$map2{$LG}}) {
		#if (undef $previous) {$previous = 0;}
		my $position1 = $map2{$LG}{$scaffold}*1000;
		my $position2 = $position1+$offset_trick;
		print KARYO "band\t$name2\_LG$LG\t$scaffold\t$scaffold\t$position1\t$position2\tgpos75\n";
		#$previous = $map1{$LG}{$scaffold};
	
	}


}
#======================================================================================================================
open (MUMMER, "<$mummer_out");
my $counter;

my %links;
print STDERR "Reading mummer file\n";
while (<MUMMER>) {
chomp;
my $hit1="fake";
my $hit2="fake";
	
	if ($_ =~ m/($prefix1.*[0-9]*\S+)\t($prefix2.*[0-9]*\S+)/) {  
		$counter++;
        if ( exists $bins1{$1}) {
			$hit1 = $bins1{$1};
		}
		
	
        if ( exists $bins2{$2}) {
			$hit2 = $bins2{$2};
		}
		
		$links{$hit1}{$hit2}++;
		
		if ($counter % 5000 == 0) {print STDERR "Read $counter Mummer matches\n"; }

	
		
		#print "$hit1\t$hit2\n";
	
	}


}

open (LINKS, ">links.txt");


foreach my $hit1 (keys %links) {
	foreach my $hit2 (keys %{$links{$hit1}}) {
	
		foreach my $LG1 (keys %map1) {
			foreach my $LG2 (keys %map2) {		
				
				if ((exists $map1{$LG1}{$hit1}) && (exists $map2{$LG2}{$hit2})) {
					my $power = $links{$hit1}{$hit2};
					if ($power > $min_contiguous_hits) {
						my $position1_start  = $map1{$LG1}{$hit1}*1000;
						my $position1_end    = $map1{$LG1}{$hit1}*1000; 
						my $position2_start  = $map2{$LG2}{$hit2}*1000;
						my $position2_end    = $map2{$LG2}{$hit2}*1000; 
						print LINKS "$name1\_LG$LG1\t$position1_start\t$position1_end\t$name2\_LG$LG2\t$position2_start\t$position2_end\n";
						#print "$power\n";
					}
				}
				
			}
		} 
	
	
	
	
	
	}
}


close (KARYO);
close (LINKS);

