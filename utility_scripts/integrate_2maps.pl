#!/usr/bin/perl

##script to build an integrated csv file from two separated csv files, input are expected to have the following format for locus name
#///   scaffold_XXX_BLOCK_YYY 	/// where XXX is the number for the scaffold and yyy is an integer number wich defines the relative position of the block in the sequece (can be either bp position or a rank)....


use strict;
use warnings;
use Data::Dumper;
#use PDL::Stats::Basic;

open (CSV1, "<$ARGV[0]");
open (CSV2, "<$ARGV[1]");
my %hash1;
my %hash1_ranked;
my %hash2;
my %hash2_ranked;
my $nind1;
my $nind2;
my @ind1;
my @ind2;

my %all_scaffolds;

####################### stage in data for map1
while (<CSV1>) {
	chomp;
	if ($_ =~ m/^id/) {
		my @line = split (",", $_);
		@ind1=@line[3..@line-1];
		$nind1= @ind1;
		next;
	}
	
	
	my @line = split (",", $_);
	
	my @suffixes = split ("_", $line[0]);
	my $scaffold = $suffixes[1];
	$all_scaffolds{$scaffold}++;
	
	my $block = $suffixes[3];
	@{$hash1{$line[1]}{$scaffold}{$block}} = ($line[2], @line[3..@line-1]); 
    
}
close (CSV1);

#print Dumper \%all_scaffolds1;

####################### stage in data for map2
while (<CSV2>) {
	chomp;
	if ($_ =~ m/^id/) {
		my @line = split (",", $_);
		@ind2=@line[3..@line-1];
		$nind2= @ind2;
		next;
	}
	
	
	my @line = split (",", $_);
	
	my @suffixes = split ("_", $line[0]);
	my $scaffold = $suffixes[1];
	$all_scaffolds{$scaffold}++; # collect also list of scaffolds, with counts 
	
	my $block = $suffixes[3]; # get block pos
	@{$hash2{$line[1]}{$scaffold}{$block}} = ($line[2], @line[3..@line-1]); # populate a big hash with all the information

}

close (CSV2);

######### suffix to individual names ############

@ind1 = map {$_.="_map1"} @ind1;
@ind2 = map {$_.="_map2"} @ind2;

#### look up for best links ########################################################

my %links_temp;
my %linked;
foreach my $chrA (keys %hash1) {
	foreach my $chrB (keys %hash2){
		foreach my $scaffold (keys %{$hash1{$chrA}}){
			if (exists $hash2{$chrB}{$scaffold}) {
				$links_temp{$chrA}{$chrB}++;
			}
		}
	}
	my $linked =(sort {$links_temp{$chrA}{$b} <=> $links_temp{$chrA}{$a}} keys %{$links_temp{$chrA}})[0];
	
    $linked{$chrA}=$linked; #get a hash representing the strongest relationship for each lg of set1 with lg of set 2, assume no ambiguity
    #print "$chrA\t$linked\n";
}
####################################################################################


### skim spurious placements########################################################


my %pos_counts1;
my %clean_pos;

foreach my $scaffold (keys %all_scaffolds) {
	foreach my $chrA (keys %hash1) {  # arbitrarily use one of the two hashes to loop on chrs
		foreach my $block (keys %{$hash1{$chrA}{$scaffold}}){ # count from set1
			$pos_counts1{$scaffold}{$chrA}++;
		}
		foreach my $block (keys %{$hash2{$linked{$chrA}}{$scaffold}}){ # and set 2 using link information
			$pos_counts1{$scaffold}{$chrA}++;
		}
	}
	my $clean_position = (sort {$pos_counts1{$scaffold}{$b} <=> $pos_counts1{$scaffold}{$a}} keys %{$pos_counts1{$scaffold}})[0];
	#print "this scaffold $scaffold is definetively in $clean_position by $pos_counts1{$scaffold}{$clean_position} occurrences\n";
	$clean_pos{$scaffold} = $clean_position; ##always refers to the chr nomencature of set1
	
}

#print Dumper \%pos_counts1;
#####################################################################################


#### generate ranking for the two maps, based on position of phase blocks ###########

foreach my $chr (keys %hash1) {
	
	foreach my $scaffold (keys %{$hash1{$chr}}) {
		my $rank =1;
		foreach my $block (sort {$a <=> $b} keys %{$hash1{$chr}{$scaffold}}) {
			$hash1_ranked{$chr}{$scaffold}{$rank} = $hash1{$chr}{$scaffold}{$block};
			$rank++;
		}
	}
}

#print Dumper \%hash1_ranked;

foreach my $chr (keys %hash2) {
	
	foreach my $scaffold (keys %{$hash2{$chr}}) {
		my $rank =1;
		foreach my $block (sort {$a <=> $b} keys %{$hash2{$chr}{$scaffold}}) {
			$hash2_ranked{$chr}{$scaffold}{$rank} = $hash2{$chr}{$scaffold}{$block};
			$rank++;
		}	
	}
}
###################################################################################


##generate empty arrays for missing blocks
my @empty1=();
my @empty2=();

for (my $i=0; $i<$nind1; $i++) {
	push (@empty1, "-");
}
for (my $i=0; $i<$nind2; $i++) {
	push (@empty2, "-");
}
####################################################

print "id,,,";
print join(",", @ind1);
print ",";
print join(",", @ind2);
print "\n";

foreach my $scaffold (keys %all_scaffolds){
	my $chr = $clean_pos{$scaffold}; #search this scaffold in the right chr given the skimmed hash
	for (my $block=1; $block<=$all_scaffolds{$scaffold}; $block++) { # cycle over many blocks as many total occurences of such scoffolds in both maps
		
		if (exists $hash1_ranked{$chr}{$scaffold}{$block} || exists $hash2_ranked{$linked{$chr}}{$scaffold}{$block} ) { #chech if blocks are over
			
			print "scaffold_$scaffold\_BLOCK_$block,$chr,0,";
			
			if (exists $hash1_ranked{$chr}{$scaffold}{$block}) {
				print join (",", @{$hash1_ranked{$chr}{$scaffold}{$block}}[1..@ind1]);
			
			}
			else {
				print join (",", @empty1);
			}
			
			print ",";
			
			if (exists $hash2_ranked{$linked{$chr}}{$scaffold}{$block}) {
				print join (",", @{$hash2_ranked{$linked{$chr}}{$scaffold}{$block}}[1..@ind2]);
			
			}
			else {
				print join (",", @empty2);
			}
			
			print "\n";
			
		}
		else {
			#last;
		}
	
	
	}

}

