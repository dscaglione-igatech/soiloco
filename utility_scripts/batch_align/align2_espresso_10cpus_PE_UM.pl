#! /usr/bin/perl -w
use strict; use warnings;
use FileHandle;
use Getopt::Long;


#bwa index -a bwtsw /share/jumbo-0-1-scratch-2/Davide/Peanut_assemblies/Assembly_correction/GapClosed.fasta.masked

#align2.pl --f=/share/jumbo-0-1-scratch-2/Davide/Peanut_reads/RILs_1st/001_PB_r1-1.fq.gz --r=/share/jumbo-0-1-scratch-2/Davide/Peanut_reads/RILs_1st/001_PB_r1-2.fq.gz --refloc=/share/jumbo-0-1-scratch-2/Davide/Peanut_assemblies/Assembly_correction/GapClosed.fasta.masked --tag=001_PB --outdir=/share/jumbo-0-1-scratch-2/Davide/Peanut_reads/test


my $reads1;
my $reads2;
my $nodetmpdir = "/share/michelmore-scratch/Davide/gr2_tmp";
my $outdir;
my $refloc;
my $tag;

GetOptions('f=s'=>\$reads1, 'r=s'=>\$reads2, 'outdir=s'=>\$outdir, 'tag=s'=>\$tag, 'refloc=s'=>\$refloc);

system("mkdir -p $outdir");
system("mkdir -p $outdir/align");
system("mkdir -p $outdir/sge");
system("mkdir -p $outdir/logs");
system("mkdir -p $outdir/split");

# Construct header ------------ #
my $CMD = "#!/bin/sh\n";
#$CMD .= "#\$ -S /bin/bash\n";
#$CMD .= "#\$ -l mf=3.7G\n";
#$CMD .= "#\$ -l jumbo\n";
#$CMD .= "#\$ -pe threaded 2\n";
#$CMD .= "#\$ -l h_rt=8:00:00\n";
#$CMD .= "#\$ -N align\n";
#$CMD .= "#\$ -e $outdir/logs/\n";
#$CMD .= "#\$ -o $outdir/logs/\n";
$CMD .= "source ~/.profile\n";
# ----------------------------- #
	
# Create directories ------------------------------------- #
$CMD .= "sleep 5\n";
$CMD .= "mkdir -p $nodetmpdir; chmod 770 $nodetmpdir;\n";
$CMD .= "mkdir -p $nodetmpdir/reads\n";
$CMD .= "mkdir -p $nodetmpdir/align\n";
$CMD .= "mkdir -p $nodetmpdir/split\n";
# -------------------------------------------------------- #

# Stage in data ---------------------- #
#$CMD .= "cp $reads1 $nodetmpdir/reads/${tag}-1.fq\n";
#$CMD .= "cp $reads2 $nodetmpdir/reads/${tag}-2.fq\n";
#$CMD .= "sleep 5\n";
# ------------------------------------ #

# decompress --- #
#$CMD .= "gunzip -fc $nodetmpdir/reads/${tag}-1.fq.gz > $nodetmpdir/reads/${tag}-1.fq\n";
#$CMD .= "gunzip -fc $nodetmpdir/reads/${tag}-2.fq.gz > $nodetmpdir/reads/${tag}-2.fq\n";
# -------------- #

# bwa aln ---------- #
#$CMD .= "bwa aln -t 10 $refloc $nodetmpdir/reads/${tag}-1.fq > $nodetmpdir/align/${tag}-1.sai\n";
$CMD .= "bwa aln -t 10 $refloc $reads1 > $nodetmpdir/align/${tag}-1.sai\n";

$CMD .= "sleep 10\n";
#$CMD .= "bwa aln -t 10 $refloc $nodetmpdir/reads/${tag}-2.fq > $nodetmpdir/align/${tag}-2.sai\n";
$CMD .= "bwa aln -t 10 $refloc $reads2 > $nodetmpdir/align/${tag}-2.sai\n";
$CMD .= "sleep 10\n";
$CMD .= "\n";
# ------------------ #


# bwa sampe -------- #
$CMD .= "bwa sampe $refloc $nodetmpdir/align/${tag}-1.sai $nodetmpdir/align/${tag}-2.sai $reads1 $reads2 > $nodetmpdir/align/${tag}.sam\n";
$CMD .= "sleep 5\n";
$CMD .= "rm $nodetmpdir/reads/${tag}-2.fq; rm $nodetmpdir/reads/${tag}-1.fq; rm $nodetmpdir/reads/${tag}-2.fq.gz; rm $nodetmpdir/reads/${tag}-1.fq.gz\n";
$CMD .= "rm $nodetmpdir/align/${tag}-2.sai; rm $nodetmpdir/align/${tag}-1.sai\n\n";
# ------------------ #

$CMD .= "sleep 5\n";
$CMD .= "samtools view -S -H $nodetmpdir/align/${tag}.sam > $nodetmpdir/align/${tag}.header\n";
$CMD .= "sleep 5\n";
$CMD .= "cat $nodetmpdir/align/${tag}.sam | grep \"XT:A:U|XT:A:M\" > $nodetmpdir/align/${tag}_unique_tmp.sam\n";
$CMD .= "sleep 5\n";
$CMD .= "cat $nodetmpdir/align/${tag}.header $nodetmpdir/align/${tag}_unique_tmp.sam > $nodetmpdir/align/${tag}_unique.sam\n";
$CMD .= "sleep 5\n";
$CMD .= "rm $nodetmpdir/align/${tag}_unique_tmp.sam; rm $nodetmpdir/align/${tag}.header; rm $nodetmpdir/align/${tag}.sam\n";
$CMD .= "sleep 5\n";
$CMD .= "samtools view -f 2 -q 1 -bS -o $nodetmpdir/align/${tag}.bam $nodetmpdir/align/${tag}_unique.sam\n";
$CMD .= "sleep 5\n";
$CMD .= "rm $nodetmpdir/align/${tag}_unique.sam\n";
# ------------------------------- #

# Sort the bam --------------- #
$CMD .= "samtools sort $nodetmpdir/align/${tag}.bam $nodetmpdir/align/${tag}.sorted\n";
$CMD .= "sleep 5\n";
$CMD .= "rm $nodetmpdir/align/${tag}.bam\n\n";
# ------------------------------- #

# Remove PCR dups --------------- #
$CMD .= "samtools rmdup $nodetmpdir/align/${tag}.sorted.bam $nodetmpdir/align/${tag}.sorted.rmdup.bam\n";
$CMD .= "sleep 5\n";
$CMD .= "rm $nodetmpdir/align/${tag}.sorted.bam\n\n";
# ------------------------------- #


# Clean up ----------------- #
$CMD .= "cp $nodetmpdir/align/${tag}.sorted.rmdup.bam $outdir/align/\n";
$CMD .= "sleep 10\n";
$CMD .= "rm $nodetmpdir/align/${tag}.sorted.rmdup.bam\n";
$CMD .= "sleep 10\n";
$CMD .= "\n\n\n";
# -------------------------- #

# Write job script ------------------- #
open(JOB, ">$outdir/sge/align-$tag.sh");
print JOB $CMD;
close(JOB);
system("sh $outdir/sge/align-$tag.sh");
# ------------------------------------ #



