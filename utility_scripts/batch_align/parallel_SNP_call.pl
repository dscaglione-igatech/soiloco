#! /usr/bin/perl -w
use strict; use warnings;
use FileHandle;
use Getopt::Long;


my $BAM;
my $nodetmpdir = "/share/michelmore-scratch/Davide/scripts_tmp";
my $outdir;
my $tag;
my $refloc;

GetOptions('BAM_folder=s'=>\$BAM_folder, 'outdir=s'=>\$outdir, 'tag=s'=>\$tag, 'refloc=s'=>\$refloc, 'CPUs=i'=>\$cpus, 'Max_Coverage=i'=>\$maxcov, 'Min_Coverage=i'=>\$mincov, 'MAPQ'=>\$mapq, 'QUAL'=>\$qual,);

system("mkdir -p $outdir");
system("mkdir -p $outdir/shs");
system("mkdir -p $outdir/stats");

my $PWD = `pwd`;


print "$PWD";
chomp $PWD;

my $CMD = "#!/bin/sh\n";

$CMD .= "source ~/.profile\n";

$CMD .= "mkdir -p $nodetmpdir; chmod 770 $nodetmpdir\n";
$CMD .= "mkdir -p $nodetmpdir/${tag}\n";

$CMD .= "perl ~/tools/scripts/get_fasta_stat.pl $refloc > $nodetmpdir/${tag}/fasta.STATS\n";

$CMD .= "perl -MList::Util -e 'print List::Util::shuffle <>' $nodetmpdir/${tag}/fasta.STATS > $nodetmpdir/${tag}/fasta.STATS.shuffled\n";


$CMD .= "sed -i 's/\(scaffold_[0-9]*\)\t\([0-9]*\)/\1\t1\t\2/' $nodetmpdir/${tag}/fasta.STATS.shuffled\n"; 

$CMD .= "split -l $chunk $nodetmpdir/${tag}/fasta.STATS.shuffled split\n";




$CMD .= " ~/tools/parallel-20120822/src/parallel -P $cpus \"samtools mpileup -q 27 -Iu -f /share/jumbo-0-1-scratch-2/Davide/Artichoke/HiSeq/assemblies/Art_allpath1/final.contigs.fasta.masked `ls -d -1 ../../../Art_pool_1/combined_run1/contigs_mask_align_1/align/*.bam` `ls -d -1 ../../../Art_pool_2/contigs_mask_align_1/align/*.bam` | /home/scaglione/tools/samtools-0.1.18/bcftools/bcftools view -Nv - | /home/scaglione/tools/samtools-0.1.18/bcftools/vcfutils.pl varFilter -D 100 -Q 20 - > Progeny_q27Q20D100.vcf\" ::: split*\n";



open(JOB, ">$outdir/shs/parSNPs-$tag.sh");
print JOB $CMD;
close(JOB);
system("sh $outdir/shs/parSNPs-$tag.sh");





