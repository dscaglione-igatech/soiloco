#!/usr/bin/env/python
import csv, os, sys

#min_snp = sys.argv[2]
infile = sys.argv[1]

conversion = {"A" : "AA",
              "B" : "BB",
              "H" : "AB",
              "-" : "-"}


f = open(infile, 'r')

csvreader = csv.reader(f, delimiter=',')

for row in csvreader:
    if len(row) == 0:
        continue
    if row[0] == "id":
        print ",".join(row)
        continue
    #print "haloa"
    #print row
    marker = row[0]
    LG = row[1]
    pos = row[2]
    fields = row[3:]
    
    
    head = [marker, LG, pos]
    conv = lambda x : conversion[x]
    new_fields = map(conv, fields)
    #print new_fields
    line = head + new_fields
    #print line
    string = ",".join(line)
    print string
