#####################################
#            SMOOTH.pl              #
#                                   #
#        Davide Scaglione           #
#                                   #
# Please acknowledge if you use it! #
#####################################
#!/usr/bin/perl

use strict;
use warnings;
use Getopt::Long;
use Pod::Usage;
use Data::Dumper;
use Try::Tiny;
my $input;
my $output;
my $Record;
my $err_threshold = 0.75;
my $majority_rule = 0.75;
my $help;
GetOptions('input=s'=>\$input, 'output=s'=>\$output, 'RECORD-format'=> \$Record, 'error-threshold=f' => \$err_threshold, 'majority-rule=f' => \$majority_rule);
#

Pod::Usage::pod2usage( -verbose => 2 ) if ( $help || !$input ||!$output );

#$err_threshold = 1 - $err_threshold;

open (DATA, "<$input");

chomp (my $line = <DATA>);


## initialize score array ##
my @weight_for=qw(0.998 0.981 0.934 0.857 0.758 0.647 0.537 0.433 0.342 0.265 0.202 0.151 0.112 0.082 0.059);
my @weight_rev=reverse (@weight_for);


## load the header######
my @samples = splice (@{[split(",", $line)]},3);  				# one-liner to split only sample names (not really necessary)
my @firstline = split(",", $line);
																#foreach my $element (@firstline) {printf ("%s\n", $element);}  # control line for header parsing
																#my @samples = @firstline[3..@firstline-1];
																#foreach my $element (@samples) {printf ("%s\n", $element);}  # control line for header parsing
##load the data#########



my @AoA = map {chomp; [split /,/]} <DATA>; ### map chomp and split over filehadler to generate my AoA (having excluded the header before)

#my $arraysize= @AoA;
#print "====$arraysize=====\n";

##reprint the entire dataset without modification
# print join(",", @firstline)."\n";
# for (my $i=0;$i<@AoA; $i++) {
	# for (my $y=0 ; $y<@{$AoA[$i]} ; $y++){
		# print $AoA[$i][$y];
		# print "," unless ($y == @{$AoA[$i]}-1);
	# }
	# print "\n";
# }
###############


for (my $i=0;$i<@AoA; $i++) {  #cycles over markers

	
	for (my $y=3; $y<@{$AoA[$i]}; $y++) {   #cycles over individuals
		
		
		my @before_weight;   # initialize two arrays for weights I need to use at each position, the might be a truncation from originals weights at borders
		my @after_weight;
		
		
		my @before_score=();  # initialize also arrays to score the flanking scoring at each position
		my @after_score=();
		
		my $el = $AoA[$i][$y]; 
		#printf ("%s\t%d\t%d\n", $el, $i, $y); # print the position under analysis
		############################################################################################
		##populate before arrays
		############################################################################################
		##upper border case#########
		
		if (($i < 15) && ($i > 0)){				# 15-> 16 elemento -> ho tutti e 15 i precedenti...Se $i � zero salto perch� la sezione non deve esistere
			#print "prova1\n";
			for my $a (0..$i-1) {				# genero array di scoring da 0, ovviamente alla posizione precedente loopando sul AoA originale
				push (@before_score, $AoA[$a][$y]);		
			}
			@before_weight = @weight_rev[@weight_for-$i..@weight_for-1]; #parallelamente l 'array di pesi e' generato dal suo originale
			#print "before is sliced\n";
		}
		##inside case##############
		
		elsif ($i != 0) {							# a questo punto controllo solo che sia maggiore di 0 per non entrarci alla prima riga
			for my $a ($i-15..$i-1){
				push (@before_score, $AoA[$a][$y]);
			}
			@before_weight = @weight_rev;            #il mio array before � tutto l'originale (sempre escludendo la prima riga...dove non viene ad esistere) 
			#print "before is full\n";
		}
		
		############################################################################################
		##populate after arrays
		############################################################################################
		##bottom border case
		
		
		if (($i > @AoA-16) && ($i < @AoA-1)) {                 # (es @AoA=100, AoA-15=85....$i=85 � 86 posizione, la prima che non pu� sfruttare array after intero
			for my $a ($i+1..@AoA-1) {							# raccolgo posizioni da i+1 alla fine
				push (@after_score, $AoA[$a][$y]);
			}
			@after_weight=@weight_for[0..@AoA-$i-2];			# array weights da 0 a 
			#print "after is sliced\n";
		}
		##inside case
		elsif ($i != @AoA-1) {
			for my $a ($i+1..$i+15) {
				push (@after_score, $AoA[$a][$y]);
			
			}
			@after_weight=@weight_for;
			#print "after is full\n";
		}
		
		
		
		###########################################################################################	
		
		my @window;
		my @weights;
		##ok here we have the two arrays initializated , unless at the very end or begin of file where one is missin
		
		if ($i == 0) {
			@window  = @after_score;
			@weights = @after_weight;
		}
		elsif ($i == @AoA-1) {
			@window  = @before_score;
			@weights = @before_weight;
			}
		else {
			@window  = (@before_score, @after_score);
			@weights = (@before_weight, @after_weight);
		}
	
		#print join (",",@window)."\t".join (",",@weights)."\n";
		
		#print Dumper \@window;
		#print Dumper \@weights;
		
		##correction routine
		
		my $sum_of_scores=0;
		my $weights_counter=0;
		my $multiplier;
		my %hash_of_memory;
		
		
			
			
		for (my $s = 0; $s<=$#window; $s++ ){
			if ($window[$s] eq "-"){
				$multiplier=0;
				#print "checkbox";
			}
			
			if ($el eq $window[$s]){ 
				$multiplier=0;
			}
			else {
				$multiplier=1;
			}
			
			my $score = $weights[$s]*$multiplier;
			
			$sum_of_scores += $score;   
			$weights_counter += $weights[$s];
			$hash_of_memory{$window[$s]}++;
		
		}
			
			
		################################

		
		my $Grandscore = $sum_of_scores / $weights_counter;

		
		
		#print $Grandscore."\t";
		foreach my $key (keys %hash_of_memory) {
			#print $key."\t".$hash_of_memory{$key};
			#print "\n";	
		}
		################################
		my @rank=();
		my $substitute="init";
		foreach my $key (sort {$hash_of_memory{$b} <=> $hash_of_memory{$a}} keys %hash_of_memory) {
			#print $key."\n";
			push (@rank, [($key, $hash_of_memory{$key})]);

			
			
		}
		
		
		##
		############################### Rank hash -> array with two values: genotype (eg. AA) and count (eg. 12)
		
		if (@rank == 1){
			$substitute = $rank[0][0];
			#$substitute = "gigi";
		}
		##next line (in case the rank is more  than one genotype) chek for a majority rule and that the highest ranked genotype is the same as
		# the previous and the next in the original matrix
		
		
		elsif ((($rank[0][1] / ($rank[1][1]+$rank[0][1])) > $majority_rule) and 
				$rank[0][0] ne "-") {
			
			$substitute = $rank[0][0];
			#$substitute = "gigi2";
			#so far collection majority rule genotype, now evaluate justapoxed loci
			# if ((defined $AoA[$i][$y-1]) && (defined $AoA[$i][$y+1])){
				# if (($rank[0][0] eq $AoA[$i][$y-1]) && ($rank[0][0] eq $AoA[$i][$y+1])){
					# # do nothing and mantain the $substiute set to rank[0][0] 
				# }
				# else{
					# $substitute = "-";
				# }
			# }
			
		}
		### in any case, reject if majority rule is not supported
		elsif (@rank == 2 && ($rank[0][0] eq "-" || $rank[1][0] eq "-") ) {
			$substitute = $el;
		}

		else {
			$substitute = "-";
		
		}
			
		#############################################
		# for debugging
		# if ($AoA[$i][$y] eq 'BU96') {
		
			# print Dumper \@rank;
			# print "Grandscore: ".$Grandscore."\n";
			# print "Threshold: ".$err_threshold."\n";
			# print "Substitute: ".$substitute."\n";
		# }
		
		
		if ($Grandscore > $err_threshold) {
			#print "I'm changing value\n";
			$AoA[$i][$y] = $substitute; #unless $substitute eq '-';	
		}
		
		#print "Putative sobstitute: $rank[0][0]\n";
		
		#print "\n";
		
		
	
	}
}

open (OUTPUT, ">$output");

print OUTPUT join(",", @firstline)."\n";
for (my $i=0;$i<@AoA; $i++) {
	for (my $y=0 ; $y<@{$AoA[$i]} ; $y++){
		print OUTPUT $AoA[$i][$y];
		print OUTPUT"," unless ($y == @{$AoA[$i]}-1);
	}
	print OUTPUT "\n";
}
#sleep 3;
close (OUTPUT);

if ($Record){

	my $RCMD = "library(qtl);";
	$RCMD.= "mapthis <- read.cross(\"csvr\", \".\", \"$ARGV[0].smoothed\", genotypes=c(\"AA\",\"AB\",\"BB\"));";  ## order of genotypes matters!!
	$RCMD.= "write.cross(mapthis, format=\"mm\", filestem=\"$ARGV[0].smoothed.mm\");";
	$RCMD.= "quit(save=\"no\", status = 0)";

	open(R," | R --slave");

	print R $RCMD;

	close(R);

	#sleep 5;
	open (MM,"<$ARGV[0].smoothed.mm.raw") or die("Cannot open file $ARGV[0].smoothed.mm.raw");

	my $stem;
	if ($ARGV[0] =~ /(.*)\.csv/) {
		$stem = $1;
	}
	my $LG = (split(/\./,$stem))[0];


	open (RECORD, ">$stem.MM.loc");
	my @MM1 = split (" ", <MM>);
	my @MM2 = split (" ", <MM>);

	my $datatype= uc $MM1[2];

	print RECORD "name = $LG.round2\npopt = $datatype\nnloc = $MM2[1]\nnind = $MM2[0]\n";

	while (<MM>) {
		if ($_ =~ /^\*id/){
			last;
		}
		else{
			print RECORD $_;
		}

	}

}

=head1 NAME

SMOOOTH.pl

=head1 SYNOPSIS

Use:

perl SMOOTH.pl   --input <sorted csvr file> --output <output csvr> [OPTIONS] [--RECORD-format]

perl SMOOTH.pl    --help      


=head1 DESCRIPTION

This script is inspired by SMOOTH, the method/software proposed by H van Os et al., 2005. It reads an ordered csvr file (R/QTL format).
It corrects errors and fills missing data on the basis of the context using a score matrix and a safe majority rule calculated on a sliding window of up tp 30 markers (less when operating at the begin or the end of the linkage group).
 
IMPORTANT: csvr file must contain a single linkage group with position-sorted markers (use sort_csv.pl for this purpose).

IMPORTANT: R must be in $PATH and library "qtl" installed.

=head1 ARGUMENTS

SMOOTH.pl  takes the following arguments:

=over 4

=item help
  --help

  Displays the usage message.


=item Input csvr file

  --input <sorted map csvr>

(Required.) Provide the csvr file (R/QTL format) for a single linkage group with markers sorted by position

=item output

  --output <corrected map csvr>

Required.) Provide a name for the output file

=head1 OPTIONS

=item RECORD format

  --RECORD-format

(Optional.) Also output marker in Record format. Useful for F2 populatons since MSTMap does not support this type of segregation.

=item error threshold

  --error-threshold (Default: 0.75)

(Optional.) This value specify the certainty of error in the dataset. It uses a scoring system based on flanking markers with weigths that decrease geometrically.
Lower this value to make the algortitm to correct more aggressively (i.e. 0.50).


=item majority rule

  --majority-rule (Default: 0.75)

(Optional.) Together with the scoring system a majority rule is also applied to accept correction of datapoint. The value reflect the percentage of fkanking markers in the given window (15 above, 15 below) that need to corroborate before accepting the imputation of the new genotype.

=head1 AUTHOR

Davide Scaglione, E<lt>davide.scaglione.agct@gmail.comE<gt>.

=head1 COPYRIGHT

This program is distributed under GNU GPL. Please cite 

=head1 DATE

6-Jun-2013

=cut

