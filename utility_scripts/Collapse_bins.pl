#!/usr/bin/perl

##This scrips takes a bin file from a ROUND2 of mapping (SMOOTH -> RECORD -> Record2csvr.pl) and make a lookup to other bin files from the previous 
##  run of the same linkage group(s). Can work on concatenated daset as long as the new and the previous sets are consecutive eachothers
## (no other ordering steps in between)

die "USAGE: $0 <new bin file> <previous bin 1> [<previous bin 2>] [previous bin 2] [previous bin n]" unless @ARGV >= 2;  

use strict;
use warnings;
my %NEW_bins; # to store data from the new bin file
my %OLD_bins; # to collect arrays of bins from previous bin (also more than one, but not overlapping each other)
my %OUT_bins;
my $checknew_scaff;
my $checknew_bins;
my $checkold_scaff;
my $checkold_bins;
my $checkout_bins;
my $checkout_scaff;



##read the ROUND2 bin file and generate a Hash_of_array (reference markers as key, array of all markers as value)

open (NEW, "<$ARGV[0]");

while (<NEW>){
	chomp;
	$checknew_bins++;
	my @temp_array = split ("\t", $_);
	$checknew_scaff += shift (@temp_array);                      # shifting because the first field is the number of scaffold in the bin
	@{$NEW_bins{$temp_array[0]}} = @temp_array;  # store first scaffold as key and all as array in value
}
close (NEW);
print STDERR "Read $checknew_scaff scaffolds from current bin set file\n";
print STDERR "Read $checknew_bins bins from current bin set file\n";

for (my $i=1; $i<@ARGV; $i++) {

	open (OLD, "<$ARGV[$i]");
	
	while (<OLD>) {
		chomp;
		$checkold_bins++;
		my @temp_array = split ("\t", $_); 
		$checkold_scaff += shift (@temp_array);				# shifting because the first field is the number of scaffold in the bin
		@{$OLD_bins{$temp_array[0]}} = @temp_array;	
	}
	close (OLD);
	print STDERR "Read $checkold_scaff scaffolds from previous file\n";
	print STDERR "Read $checkold_bins bins from previous file\n";
	
}


foreach my $main_key (keys %NEW_bins){                 #cycles over the hash of new bin file

	foreach my $myscaffold (@{$NEW_bins{$main_key}}){        # the inside each value as array
		#print $myscaffold."\n";
		if (exists $OLD_bins{$myscaffold}) {						#if this scaffold is in the old bins hash...and it must be...
			#print "I'm happy\n";
			push (@{$OUT_bins{$main_key}}, @{$OLD_bins{$myscaffold}});   # remove the single value from the hash (the new one) and replace with the set that was in the old array
			#push  (@{$NEW_bins{$main_key}}, @{$OLD_bins{$myscaffold}});
		
		}
		else{
			#print "Not happy\n";
			#die "Marker $myscaffold of the NEW bin set is not present as representative one in the precursor dataset!! Some data is missing\n";
		}
		
	}
}

foreach my $sample_scaff (keys %OUT_bins) {
	$checkout_bins++;
	print scalar @{$OUT_bins{$sample_scaff}}."\t";
	$checkout_scaff += scalar @{$OUT_bins{$sample_scaff}};
	
	for (my $i = 0 ; $i < @{$OUT_bins{$sample_scaff}}; $i++) {
	
		print @{$OUT_bins{$sample_scaff}}[$i]."\t";
	
	}
	print "\n";

}

print STDERR "Wrote $checkout_bins bins of $checkout_scaff scaffolds\n";