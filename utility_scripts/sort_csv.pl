## Davide Scaglione ##

#!/usr/bin/perl
##This script simply read a csvr file as generated from R/QTL and order it on the basis of LG and position
##First line must contain header with sample names (id,,,sample1,sample2,...)


use strict;
use warnings;

open (DATA, "<$ARGV[0]");
my $firstline = <DATA>;

my @sorted = map {$_->[0]}
             sort { $a->[2] <=> $b->[2] ||
                    $a->[3] <=> $b->[3] }
             map {chomp;[$_,split(/,/)]} <DATA>;


print "$firstline";			 
print "$_\n" for @sorted;

close (DATA);