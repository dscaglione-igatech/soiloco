
import re
import sys
import csv
import math
import argparse
import ipdb


class Marker(object):

    def __init__(self, name, calls, **kwargs):
        self.name = name
        self.calls = calls
        self.matches = []
        self.LG = kwargs.get('LG', "0")
        self.pos = kwargs.get('pos', "0")
        self.switched = False
        
    def add_match(self, match):
        self.matches.append(match)
     
    def filter_by_LG(self, LG):
        for match in self.matches:
            if match.LG == LG: yield match


class Match(object):
    def __init__(self, subject_name, LG, pos, matches, mismatches, missed, hets):
        self.subject = subject_name
        self.LG = LG
        self.pos = pos
        self.matches = matches
        self.mismatches = mismatches
        self.missed = missed
        self.hets = hets
        self.score = (self.matches - self.mismatches) * (1 - math.log(1+(self.hets/self.matches)))
        self.ident = 1.0 * self.matches / (self.matches + self.mismatches)
        #self.switched = switched

def hamming(list1, list2, **kwargs):
    missing = kwargs.get('missing_char', '-')
    missed = matches = mismatches = hets = 0.0
    for a, b in zip(list1, list2):
        if (a == missing or b == missing):
            missed += 1
        elif(a == b):
            if a == "AA" or a == "BB" or a == "A" or a == "B":
                matches += 1
            else:
                matches += 1
                hets += 1
        else:
            mismatches +=1
            
        #print a + "\t" + b + "\n"
    return (float(matches), float(mismatches), float(missed), float(hets))


def switch_cp(marker):
    trans = {"AA" : "AB",
             "AB" : "AA",
             "H" : "A",
             "A" : "H",
             "-" : "-",
             "--" : "-",
             "?" : "?"}
    for i in range(0, len(marker.calls)):
        marker.calls[i] = trans[marker.calls[i]]
    marker.switched = True

if __name__ == '__main__':
    
    parser = argparse.ArgumentParser(description='Map binner')
    parser.add_argument('--map', dest='map', required=True,
                        help='Input map in csvr format')
    parser.add_argument('--to-bin', dest='to_bin',
                        help='Markers to bin in csvr format')
    parser.add_argument('--pre-bins', dest='pre_bins',
                        help='Previously binned markers' + \
                        'first column is number of markers, second is the master marker,' + \
                        'following markers are the binned markers')
    parser.add_argument('--out-prefix', dest='prefix', required=True,
                        help='Output prefix')
    
    my_args = parser.parse_args()
    
    infile1 = my_args.map ## file of mapped markers
    infile2 = my_args.to_bin ## file of markers to bin
    inprebin = my_args.pre_bins
    prefix = my_args.prefix  ## prefix of output file
    generate_fake_bins = False
    
    
    outreport = open (prefix + "bin_mapped.report", "w")
    outbins = open (prefix + "bin_mapped.bins", "w")
    
    ### get previous bins or generate them ###
    prebin = {}
    if inprebin:
        prebin_reader = csv.reader(open(inprebin, "r"), delimiter="\t")
        for row in prebin_reader:
            row = filter(lambda x: x != "", row)
            prebin[row[1]] = row[2:]
    else:
        print "Generating fake bins from map file"
        generate_fake_bins = True
            
    #print prebin
    ### initiate report writer    

    
    
    
    header = "scaffold\tmatch\tLG\tposition\tscore\tidentity\tswitched\n"
    outreport.write(header)

###################   read file 1, map ################################
    file1 = open(infile1, 'r')
    file1reader = csv.reader(file1, delimiter=',')
    map_markers = []
    LG_list = {}
    bin = {}
    
    for row in file1reader:
        if row[0] == "id":
            continue
        else:
            marker = Marker(row[0], row[3:], LG=row[1], pos=row[2])
            if not row[1] in LG_list.keys():
                LG_list[row[1]] = 1
            else:
                LG_list[row[1]] += 1
                 
            map_markers.append(marker)
            bin[row[0]] = []
            
            if generate_fake_bins:
                prebin[row[0]] = []
            
            
    file1.close()
##################   read merkers to be binned ########################
    try_switching = False
    tobin_markers = []        
    file2 = open(infile2, 'r')
    file2reader = csv.reader(file2, delimiter=',')
    for row in file2reader:
        if row[0] == "id":
            continue
        else:
            marker = Marker(row[0], row[3:], LG=row[1], pos=row[2])
            tobin_markers.append(marker)
            if try_switching:
                marker = Marker(row[0], row[3:], LG=row[1], pos=row[2])
                switch_cp(marker)
                tobin_markers.append(marker)
                
                
    file2.close()        
#######################################################################
    missing_char = '-'


###############################  populate list of possible matches ####
    for query in tobin_markers:
        for subject in map_markers:
            
            (matches, mismatches, missed, hets) = hamming(query.calls, subject.calls, missing_char=missing_char)
            tot = matches + mismatches + missed
            ## first skim to store matches
            if matches > 15 and (mismatches == 0 or matches/mismatches > 1.5):
                query.add_match(Match(subject.name, subject.LG, subject.pos, matches, mismatches, missed, hets))
            
            #print str(matches) + " " + str(mismatches) + " " + str(missed)

#######################################################################

    for query in tobin_markers:
        LG_best =[]
        for LG in LG_list.keys():
            #print LG
            matches = query.filter_by_LG(LG)
            try:
                LG_best.append(sorted(matches, key=lambda x:x.score, reverse=True)[0])
            except IndexError:
                continue
        
        print len(LG_best)
        
        if len(LG_best) == 0:
            print ("%s can't find a any match") % query.name
            continue
        elif len(LG_best) > 1:    
            #print "Two LGs!"
            first = sorted(LG_best, key=lambda x:x.score, reverse=True)[0]
            second = sorted(LG_best, key=lambda x:x.score, reverse=True)[1]

            if (first.score > second.score * 1.5 or
                first.ident - second.ident > 0.10):
                pass
            
            else: 
                
                print "Two ambiguous LGs: " + query.name
                print first.LG + "\t" + second.LG
                print str(first.ident) + "\t" + str(second.ident)
                continue
            
            
            if  not(first.ident >= 0.75):
                print "Identity below threshold"
                continue
            

        else:
            first = LG_best[0]
    
        if first.score >= 15:
            bin[first.subject].append(query.name)
            #header = "scaffold\tmatch\tLG\tposition\tscore\tidentity\tswitched\n"
            report = [query.name, first.subject, first.LG, str(first.pos), 
                      str(first.score), str(first.ident), str(query.switched)]
            calls_string = "\t".join(query.calls)
            outreport.write("\t".join(report) + "\t" + calls_string + "\n")
    
    for framework in bin.keys():
        prebin[framework] += bin[framework]
        num = len(prebin[framework]) + 1
        to_write = str(num) + "\t" + framework + "\t" + "\t".join(prebin[framework])
        outbins.write(to_write + '\n') 
    
    ipdb.set_trace()
 
      