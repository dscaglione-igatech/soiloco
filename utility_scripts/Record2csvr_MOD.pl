#!/usr/bin/perl

use warnings;
use strict;
use Getopt::Long;
use Data::Dumper;

my $tag = "scaffold_";
my $maxrec = 0.01;
my $MSTpatch;
## This script takes as input a RECORD output file and uses position information to update a csvr file from RQTL, parallely, it also generates in STDOUT
## a cumulative report of bins, by looking at duplicates removed by records itself and markers that felt on the exact same position (since RECORD does not
## cosider duplicates if there is a missing data breaking the perfect identity
## Can work on a concatenated file as long as each linkage group is represented only once.

## Note the first element of a bin is the one reported in the map

die "Usage 
$0 <concatenated RECORD result file> <csvr  'R/qtl' file to update> <updated csvr> [options]
--max_rec 			maximum distance in cM to collapse loci in a single bin [$maxrec]
--MST-patch			use for MSTmap order files (i.e. where no binning is given). Will re-print all markers in a bin
" unless @ARGV > 2; 



GetOptions('max_rec=f'=>\$maxrec, 'MST-patch'=>\$MSTpatch);

open (RECORD, "<$ARGV[0]");
my %map_loci;
my $state=0;
my $read=0;
my $group_n=0;
my @pos_array;
my %bin_hash;
my $prev_pos = -1;
my $prev_locus = "";

if ($MSTpatch) {
	open (RECORD, "<$ARGV[0]");
	while (<RECORD>) {
		if ($_ =~ /(group UP|;BEGINOFGROUP)/) {
			$read = 1;
			next;
		}
		## populate an hash with fake bins, with one marker	
		if (($_ =~ /(\S+)\s+\d+\.\d+/ ) and ($read == 1)){
			my @temp_array = ($1);
			@{$bin_hash{$1}} = @temp_array;
			next;
		}
		if ($_ =~ /(group _Down|;ENDOFGROUP)/) {
			$read = 0;
			next;
		}
	}


	close (RECORD);
}

#print Dumper \%bin_hash;

open (RECORD, "<$ARGV[0]");
while (<RECORD>) {

	$_ =~ s/\*//g;
	
	if ($_ =~ /\s+Bins:/) {$state = 1; $group_n++;}								# activate identity bins recording 
	if ($_ =~ /marker names after removal of duplicates:/) {$state = 0;}		# deactivate identity bins recrding
	if ($_ =~ /(group UP|;BEGINOFGROUP)/) {$state = 2;}							# activate position bin recording
	if ($_ =~ /(group _Down|;ENDOFGROUP)/) {$state = 0;}							# deactivate position bin recording 
	
	
	#create hashed collection of non redundat loci by RECORD skim
	
	if (($state == 1) && not ($_ =~ /^$/) && not ($_ =~ /\s+Bins:/)) {    #this is to match the multi-locus lines of bins that were already determined by Record
												## WILL NEVER ENTER THIS IF USING MST output
		
		chomp;
		my @temp_array = split (" ", $_);
		#my @duplicates = @temp_array[1,-1];
		@{$bin_hash{$temp_array[0]}} = @temp_array;   
		#print "$temp_array[0]"."\t"."@{$bin_hash{$temp_array[0]}}[1]\n";
	}
	
	# do more binning by position on the map
	
	if (($state == 2) && ($_ =~ /(\S+)\s+(\d+\.\d+)/)) {
		#print "catcha!";
		if (abs($2-$prev_pos) <= $maxrec) {
		    #print scalar (@{$bin_hash{$prev_locus}})."\t";
			#print scalar (@{$bin_hash{$1}})."\t";
			
			push (@{$bin_hash{$prev_locus}}, @{$bin_hash{$1}});   # if binned with the previous, push it on the prev locus hash and delete it's own hash
			#print scalar (@{$bin_hash{$prev_locus}})."\t";
			delete $bin_hash{$1};
			
		}
		
		else{
			$map_loci{$1} = $2; 			# else, it's a new bin...store it in the hash for updating the csvr file
			$prev_locus = $1;
			$prev_pos = $2;
		}
		
		if ($MSTpatch){
			$map_loci{$1} = $2;  # superimpose printout of all markers for MSTmap pipeline
			
		}
	
	}
}

close (RECORD);

open (CSVR, "<$ARGV[1]");
open (OUT_CSVR, ">$ARGV[2]");

# generate a new, bin-reduced csvr file for R/Qtl


my $firstline = <CSVR>;

print OUT_CSVR $firstline;

while (<CSVR>) {
	chomp;
	
	my @line = split(",", $_);
	
	if (exists $map_loci{$line[0]}) {				# only print loci present in the %map_loci hash (1 per bin)
		$line[2] = $map_loci{$line[0]};				# only update position, LG are expected to be already determined by R
		print OUT_CSVR join(",",@line)."\n";
	}
	
	else {
		#print OUT_CSVR $_."\n";
	}

}
close (CSVR);
close (OUT_CSVR);

foreach my $sample_scaff (keys %bin_hash) {
	
	print scalar @{$bin_hash{$sample_scaff}}."\t";
	
	for (my $i = 0 ; $i < @{$bin_hash{$sample_scaff}}; $i++) {
	
		print @{$bin_hash{$sample_scaff}}[$i]."\t";
	
	}
	print "\n";


}

