use strict;
use warnings;
use Getopt::Long;

use vars qw($MAF_1 $MAF_2 $vcf $prefix $max_cov $min_cov $max_strand_bias $a $b $c $d $max_tail_bias $min_qual); 


$MAF_1 = 0.30;
$MAF_2 = 0.70;
$max_cov = 200;
$min_cov = 50;
$max_strand_bias = 0.005;
$max_tail_bias = 1;
$min_qual = 100;

GetOptions('MAF-low=f'=>\$MAF_1, 'MAF-high=f'=>\$MAF_2, 'VCF=s'=>\$vcf, 'prefix=s'=>\$prefix, 
            'max-coverage=i'=>\$max_cov, 'min-coverage=i'=>\$min_cov, 'max-strand-bias=f'=>\$max_strand_bias);


my $bed_out = $prefix.".bed";
my $vcf_out = $prefix.".vcf";


open(VCF, "<$vcf");

open (BED, ">$bed_out");
open (VCF2, ">$vcf_out");


while (<VCF>) {
    chomp;
    #print $_."\n";
    if ($_ =~ m/^#/ ) {
		print VCF2 $_."\n";
        next;
    }
    
    my @line = split (/\t/, $_);
    if ($line[5] < $min_qual) {
        next;
    }
    
    my $contigID = $line[0];
	my $POS = $line[1];
    
    
    #######################################
    my $DP4 = $line[7];
    
    if ($DP4 =~ m/.*DP4=(\d+,\d+,\d+,\d+);.*/) {
        ($a, $b, $c, $d) = split(",", $1);
    
        my $cov = $a+$b+$c+$d;
        my $ref = $a+$b;
        ### testing for coverage and MAF
        if ( ($cov < $min_cov) or ($cov > $max_cov) or ($ref/$cov < $MAF_1) or ($ref/$cov > $MAF_2) ) {
            next;
            
        } 
            #print $a."\n";
    }
    
    else {
        next;
    }
    ##################################
    
    my $PV4 = $line[7];
    if ($PV4 =~ m/.*PV4=(.*)/) {
        my @values = split(",", $1);
        my $strand_bias = $values[0];
        my $tail_bias = $values[3];
        if (($strand_bias < $max_strand_bias) or ($tail_bias < $max_tail_bias)) {
            #print "STRAND BIASSSSSSSSSS\t";
            #print "$strand_bias\n";
            #print "$_\n";
            next;
        
        }
    }
    else{
        #print "WHATTTTTTTTTT\n";
        next;
    }
    #my $strand_bias = $line[7];
    
    my $minus_pos = $POS - 1; 
    print BED "$contigID"."\t"."$minus_pos"."\t"."$POS"."\n";
    print VCF2 $_."\n"
    

}

