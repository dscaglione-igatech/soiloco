#!/usr/bin/perl


use strict;
use warnings;
my $prev_pos = 123;
open (CSV, "$ARGV[0]");

while (<CSV>) {
    if ($_ =~ /^id/) {
        print $_;
        next;
    }
    chomp;
    my @line = split (",",$_);
    
    if ($line[2] == $prev_pos) {
        $prev_pos = $line[2];
        next;
    }
    else {
        print $_."\n";
        $prev_pos = $line[2];
    }
}