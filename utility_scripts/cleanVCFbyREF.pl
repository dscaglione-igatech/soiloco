#!/usr/bin/perl

# This script simply remove VCF position from target.vcf that are present in to_filter_out.vcf
#It prints new VCF to STDOUT, please redirect


use strict;
use warnings;

die "USAGE: cleanVCFbyREF.pl to_filter_out.vcf target.vcf > clean.vcf" unless @ARGV==2;

my %hash_ref;

open (REF, "< $ARGV[0]");

while (<REF>) {

	if ($_ =~ m/^#/) {next;}
	my @line_ref = split ( "\t", $_);
	my $SNP_ref = $line_ref[0]."-".$line_ref[1];
	$hash_ref{$SNP_ref} = 1;
	
}
close (PROGENY);

open (PROGENY, "<$ARGV[1]");

while (<PROGENY>) {

	if ($_ =~ m/^#/) {print "$_"; next;}
	my @line_query = split ("\t", $_);
	my $SNP_query = $line_query[0]."-".$line_query[1];
	if (exists $hash_ref{$SNP_query}) {next;}
	else { print "$_";} 
	
}
	
close (PROGENY);