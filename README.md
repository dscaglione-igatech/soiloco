# SOILoCo #
## Scaffold Ordering by Imputation with LOw COverage ##
A set of perl scripts to generate genotype calls on scaffolds using low pass sequencing data.
This tool have been developed to reconstruct the genome of *Cynara cardunculus* by genetic anchoring of scaffolds using a  low-pass sequencing strategy. The novelty of the software relies on the ability to analyze segregation pattern of parental phases in a F1 propulation where each individual is sequenced with less than 1x coverage.

The pipeline is composed of five main modules:

**1) TC_separator** takes as input SNP calls of parents and the segregating progeny (in multi-sample VCF format, generated with *samtools mpileup*) along with reconstructed phases of each parent by HAPCUT software ([https://sites.google.com/site/vibansal/software/hapcut](Link URL)). It generates a re-shaped progeny-wise VCF file for each parent (with respective test-cross SNP sites) where scaffolds are divided in phase blocks. 

*The following steps are performed for each parent separately. The pipeline is also designed to work with other types of population such as F2, RIL, BC. In these cases a single segregation VCF is used in the following steps. TCseparator is only required for CP and BC population. F2 and RIL population (given that reference and alternative nucleotides represent the two segregating haplotypes) can start directly from step 2.


**2) vcf2strings** takes as input a re-shaped test-cross VCF and generates simplified strings of genotype calls.

**3) gt-hmm** invokes the main HMM engine over strings to compute the most probable genotype by adopting LOD score filtering criterion

**4) TC_string_merger** takes care of merging and find consensus across test-cross haplotypes of the two phases

**5) calls2csvr** finally converts haplotype block calls in to a csv format for linkage analysis.


Please refer to the help message of each module for further information about parameters.
A toy dataset is provided to test pipeline functionality.
To better understand the method please refer to the paper:

* Scaglione D, Reyes-Chin-Wo S, Acquadro A, Froenicke L, Portis E, Beitel C, Tirone M, Mauro R, Lo Monaco A, Mauromicale G, Faccioli P, Cattivelli L, Rieseberg L, Michelmore RW, Lanteri S. **The genome sequence of the
outbreeding globe artichoke constructed de novo incorporating a phase-aware low-pass sequencing strategy of F1 progeny** 2016. *Scientific Reports* 6, Article number: 19427; doi:10.1038/srep19427