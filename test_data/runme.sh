#!/bin/sh

## First stage is to separate the two one way test cross segregations, also dividing alternative alleles being on phase 0 (type1) or phase 1 (type2)
perl ../soiloco_core/TC_separator.pl --HAP1 C3.subsample11.phased.hapcut --HAP2 Alt.subsample11.phased.hapcut --parents Two_parents_q25D200Q20.all.subsample11.body.vcf.gz --progeny Progeny.all.q25.D350.Q25.subsample11.vcf.gz

## output files automatically generated based on input name

###################################
# This will generate "strings" files were calls are put in a row for each individiual for any given phase block
## from here we are going to process only one parent, the second one can be done same way, the dataset is already split by testcross strategy; MAF are used as an extra filter over the progeny data; since in a one way test cross scenario we are expecting segragation ratio of 1:3 two symmetric MAF are taken as expected averages, with range being the allowed +/- boundaries of such  

perl ../soiloco_core/vcf2strings.pl C3.subsample11.phased.hapcut.testcross.vcf --MAF-1=0.25 --MAF-2=0.75 --range=0.175
## output files automatically generated based on input name

##################################

## There are probability parameters one can change in this, however I would discurage doing so, the system was at the time set to find good trade-off between specificity and sensitivity. However you can inquire on this if you want

perl ~/soiloco/soiloco_core/gt-hmm.pl --STRINGS=C3.subsample11.phased.hapcut.testcross.strings --OUTPUT=C3.subsample11.phased.hapcut.testcross.calls

###################################

## the parameter in this command are same as default, just set to show

perl ../soiloco_core/TC_string_merger.pl --HMM-results C3.subsample11.phased.hapcut.testcross.calls --individuals C3.subsample11.phased.hapcut.testcross.individuals --output C3.subsample11.phased.hapcut.testcross.calls.merged  --Max-missing 0.30 --Min-H-ratio 0.35 --Min-consensus 0.50


###################################


# Finally transform the data in R/QTL readable format doing some filtering (alpha is the pvalue of a chisquare for distorsion, population type must be provided to instruct the correct testing of ratios)

perl ../soiloco_core/calls2csvr.pl --HMM-results C3.subsample11.phased.hapcut.testcross.calls.merged --individuals C3.subsample11.phased.hapcut.testcross.individuals --output C3.subsample11.phased.hapcut.testcross.calls.merged.filtered.csvr --pop CP --Max-missing 0.30 --alpha 0.01

