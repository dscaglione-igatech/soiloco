#!/usr/bin/perl
#####################################
#         vcf2strings.pl            #
#                                   #
#        Davide Scaglione           #
#                                   #
#####################################
use strict;
use warnings;
use Data::Dumper;
use Getopt::Long;
use Pod::Usage;
my $MAF_1;
my $MAF_2;
my $range=0.15;
my $min_qual=20;
my $keep_mis;
my $help;

GetOptions('MAF-1=f'=>\$MAF_1, 'MAF-2=f'=>\$MAF_2, 'range=f'=>\$range, 'min-qual=f'=>\$min_qual, 'keep-missing'=> \$keep_mis, 'help'=>\$help );

Pod::Usage::pod2usage( -verbose => 2 ) if ( $help || !$MAF_1);


my $bound1_low  = $MAF_1 - $range;
my $bound2_low  = $MAF_2 - $range unless (!defined $MAF_2);
my $bound1_high = $MAF_1 + $range;
my $bound2_high = $MAF_2 + $range unless (!defined $MAF_2);

print STDERR "MAF-1 low bound:\t$bound1_low\n";
print STDERR "MAF-1 high bound:\t$bound1_high\n";
print STDERR "MAF-2 low bound:\t$bound2_low\n" unless !defined $MAF_2;
print STDERR "MAF-2 high bound:\t$bound2_high\n" unless !defined $MAF_2;

open (GENOT, "< $ARGV[0]");

my $name_out= $ARGV[0];
$name_out =~ s/\.vcf// ; # generate a file with just the names of individuals for easy parsing of HMMed output later
open (OUT, ">$name_out\.strings");

my %hash;
my $nind;
my @names;
my $prev_contig;
my $restart=0;
my $PL_pos;


sub gather_format {
    my @line = @_;
    my @FORMAT = split (":", $line[8]);
    my @index = grep { $FORMAT[$_] =~ /PL/ } 0..$#FORMAT;
    #print STDERR "PL position is parsed at $index[0]\-index FORMAT field\n";
    return $index[0];
    }

while (<GENOT>) {
	
	chomp;
	
	if ($_ =~ m/^##/ ) {
		next;
	}
	if ($_ =~ m/^#CHROM/) {
		@names= split (/\t/, $_);
		splice (@names,0,9);              		#stage in an array of sample names and remove first nine non-sample columns
		@names = map { (split("/", $_))[-1] } @names;
		
		
		open (NAMES, ">$name_out\.individuals\n");
		print NAMES (join (",", @names));
		close (NAMES);
		
		$nind=scalar(@names);					#extract the number of samples for further looping
		print "$nind\n" ;                   	#print the num of individuals
		#foreach (@names) {};					#print names
		next;
	}
		
	
	
	my @line = split (/\t/, $_);
	my $contigID = $line[0];
	my $POS = $line[1];
	
	
	################################   HASH DUMPING ROUTINE ################################################
	
	if (defined $prev_contig && length $prev_contig > 0) {					#This routine should start when the pointer is moving to a new
		if ($contigID ne $prev_contig && $restart==0) {	 					#contig and need to dump all the positions of the previous
			#print "DUMP1!!!!\n";
			my @positions;
			my @calls;
			my $indnum;
			
			for ($indnum=0; $indnum<$nind; $indnum++) {
				
				my $scaffold = $prev_contig;

				foreach my $pos (sort {$a <=> $b} keys %{$hash{$prev_contig}}) {
					if ($hash{$prev_contig}{$pos}{$names[$indnum]} =~ m/A|B|H|-/) {
						push (@positions, $pos);
						push (@calls, $hash{$prev_contig}{$pos}{$names[$indnum]});   # dump the hash in array.  
					}
				}
			
			print OUT "$prev_contig\t"."$names[$indnum]\t".join("",@calls)."\t".join(",",@positions)."\n";
			@calls=();
			@positions=();  
			}
			%hash=();
			#print "\n";
			$restart=1;
		}
		else {$restart=0;} # this is to prevent in some cases to start the dumping routine when just moving to a new contig
	}
	
	###############################################################################################
	
	
	
	################## Reading and collecting calls ###################################
	#  this is the real parser, collect data in the hash iteratively
	
	
	#looping through the individuals of one site
	
	my $AF1 = $line[7];										
	my $qual = $line[5];
	$AF1 =~ s/.*AF1?=([0-1]*\.*[0-9]*);.*/$1/;
	#print "$AF1\n";
	
	
	if (((($AF1 >= $bound1_low) && ($AF1 <= $bound1_high)) || (!defined $MAF_2 || (($AF1 >= $bound2_low) && ($AF1 <= $bound2_high)))) && ($qual >= $min_qual))  {			### only if MAF is above threshold ###
		
		#print @line;
		if (!defined $PL_pos) {
			$PL_pos = gather_format(@line);
			}
		#print $PL_pos."\n";

		for (my $i=0; $i<$nind; $i++) {     #loop through genotypes field of each individual
			my $value = ''; 				# pre-assign UNKNOWN(4)
			my $sample_field= $line[$i+9];        # stage the field in a string
			
			#print $record."\n";	
			
			if ($sample_field =~ m/0,0,0|\.\/\./ ) {       # look if the string is a missing data point (0,0,0)
				if ($keep_mis) {
					$value =  '-';
					}
				else {
					$value =  '';
				}
			}
			else {
				my $record=  (split(/:/,$sample_field))[$PL_pos];
				my @LKH =();
				@LKH = split (/,/,$record);
				if (($LKH[0] == 0) && ($LKH[1] > 0) && ($LKH[2] > 0)){
				$value = 'A';			#homozygous for REF
				}
				if (($LKH[0] > 0) && ($LKH[1] > 0) && ($LKH[2] == 0)){
				$value = 'B';			#homozygous for ALT
				}
				if (($LKH[0] > 0) && ($LKH[1] == 0) && ($LKH[2] > 0)){
				$value = 'H';			#HETEROZYGOUS
				}
			}
			
			
			$hash{$contigID}{$POS}{$names[$i]} = $value;
			$prev_contig = $contigID;
		
		}
	}
	else {$prev_contig = $contigID};
	
	#a very last dump if we hit the end of file, which will not bring back to the dump routine...need to program better next time :-/
	
	if (eof) { 
			my @positions;
			my @calls;
			my $indnum;
			
			for ($indnum=0; $indnum<$nind; $indnum++) {
				
				my $scaffold = $prev_contig;
				foreach my $pos (sort {$a <=> $b} keys %{$hash{$prev_contig}}) {
					if ($hash{$prev_contig}{$pos}{$names[$indnum]} =~ m/A|B|H|-/) {
						push (@positions, $pos);
						push (@calls, $hash{$prev_contig}{$pos}{$names[$indnum]});  
					}
				}
			
			print OUT "$prev_contig\t"."$names[$indnum]\t".join("",@calls)."\t".join(",",@positions)."\n";
			@calls=();
			@positions=();
	
			}
	}

	
	
###############################################################################################
}
close (GENOT);
=head1 NAME

vcf2strings.pl

=head1 SYNOPSIS

Use:

perl vcf2strings.pl   <multisample.vcf> --MAF-1= float [--MAF-2 float] --range= float --min-qual= int [--keep_missing]

perl vcf2strings.pl    --help      


=head1 DESCRIPTION

This script takes as input a single vcf files and covert variant calls of each individual to a string to be the input of gh-hmm.pl
In the case of CP populations the input vcf can be either one of the two TC_separator.pl outputs having variants already sorted, swithced and divided in blocks using phase information. Alternatively, for F2 and RIL populations, any vcf file having all REF nucleotides as representative of one parent, while ALT nucletides need to be representative of the other parents. 

=head1 ARGUMENTS

vcf2strings.pl takes the following arguments:

=over 4

=item help
  --help

  Displays the usage message.


=item MAF-1

  --MAF-1 float

(Required.) This is one allele frequency a site must have to be considered. Lower and higher thresholds are specified by  the --range parameter
To genotype F2 and RIL populations where 0.50 is the expected allele frequency for all sites in the progeny, this MAF is the only one required.
To genotype BC anc CP population this value must be set at 0.25 togheter with a MAF-2 set at 0.75

=item MAF-2

  --MAF-2 float

(Optional.) This a second possible allele frequency a site must have to be considered. Lower and higher thresholds are specified by  the --range parameter
Only useful in BC and CP populations where it is usually set at 0.75, given MAF-1 set to 0.25

=item range

  --range float

(Required.) This value specify the confidence intarvals for MAF-1 and MAF-2 (where set). Given --range=0.15 in a typical CP/BC analysis the boundaries to accept allele frequencies will be 0.10-0.40 for MAF-1 and 0.60-0.90 for MAF-2 (Deafult: 0.175)


=item minimum quality

  --min-qual

This value specify the minium quality required to accept a SNP site. This parameter must be carefully set on the basis of the variant calling software which generated the vcf (Default: 20)


=item keep missing

  --keep-missing

(Debug.) This switch is only used to print missing SNP calls as '-' in the strings. This allows to manually inspect strings with column-orderd positions. This output format WILL NOT WORK with gt-hmm.pl. 


=head1 AUTHOR

Davide Scaglione, E<lt>davide.scaglione.agct@gmail.comE<gt>.

=head1 COPYRIGHT

This program is distributed under GNU GPL.

=head1 DATE

6-Jun-2013

=cut


