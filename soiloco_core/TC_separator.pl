#                                   #
#         TC_separator.pl           #
#                                   #
#        Davide Scaglione           #
#                                   #
#####################################

#!/usr/bin/perl

use strict;
use warnings;
use Data::Dumper;
use Term::ProgressBar;
use Getopt::Long;
use Pod::Usage;

#This script is ment to generate two progeny files for each testcross analysis of the two parents, 
#It gathers information from the bi-perental SNP calling and the respective assmbled phases from HAPCUT
#HAPCUT_P1 and HAPCUT_P2 need to be in the same order as in the bi-parental snp calling vcf
#
#usage TC_separator.pl --HAP1=<HAPCUT_P1> --HAP2=<HAPCUT_P2> --Parents=<Two_parents.vcf> --Progeny=<Progeny_file.vcf> <maxMEC> <min_phased> <lkh> > TC_reconstructor.log


my $maxMEC;
my $min_phased;
my $lkh;
my $hap1;
my $hap2;
my $parents;
my $progeny;
my $help;
##

GetOptions('HAP1=s'=>\$hap1, 'HAP2=s'=>\$hap2, 'parents=s'=>\$parents, 'progeny=s'=>\$progeny, 'max-MEC=s'=>\$maxMEC, 'min-phased=s'=>\$min_phased, 'LKH=s'=>\$lkh, 'help'=>\$help);

Pod::Usage::pod2usage( -verbose => 2 ) if ( $help );

$maxMEC = 500 unless $maxMEC;
$min_phased = 10 unless $min_phased;
$lkh = 25 unless $lkh;


my $skip_block;
my $block_num;

#STATS
my $discarded_blocks_1;  
my $good_blocks_1;
my $count_snps_1;
my $phased_var_1;
my $discarded_blocks_2;
my $good_blocks_2;
my $count_snps_2;
my $phased_var_2;
my $progeny_discarded_SNPs;


my %P1_phase;
my %P2_phase;
my $hash_name;
my $parent1;
my $parent2;

my $nind;
my %archive;

my $filehandle;

#STATS
my $count_phased_P1;
my $count_phased_P2;
my $count_phased_P1_f;
my $count_phased_P2_f;
my $count_unphased_P1;
my $count_unphased_P2;

my $not_TC;
my $count_snps;





## read the first parent hapcut files

open (HAPCUT1, "<$hap1");
print STDERR "Reading phased SNPs P1 $hap1\n";

while (<HAPCUT1>) {

	if ($_ =~ m/^BLOCK/ ) {
		my @line = split (" ",$_);
		$block_num = $line[2];
		
		if ( ($line[10] >= $maxMEC) || ($line[6] <= $min_phased)){
			#print "blocked\n";
			$skip_block=1;
			$discarded_blocks_1++;  # count discarded blocks by MEC and min num of phased snps
			next;
		}
		
		else {
			#print "unblocked\n";
			$phased_var_1 += $line[6];
			$skip_block=0;
			$good_blocks_1++; # count good blocks
			next;
		} 
		
	}
	
	elsif ($_ =~ m/^\*/ ) {
		#$changing=1; 
		next;
	}
	
	else {
		my @line = split ("\t", $_);
		if ($skip_block != 1) {
			#print $hashes_ref[$i];
			my $coordinate = $line[3]."-".$line[4];
			@{$P1_phase{$coordinate}} = ($line[1], $block_num);
			
		}
		
		else {
			next;
		}
		
	}

}
close (HAPCUT1);


## read the second parent hapcut files

open (HAPCUT2, "<$hap2");
print STDERR "Reading phased SNPs P2 $hap2\n";
while (<HAPCUT2>) {

	if ($_ =~ m/^BLOCK/ ) {
		my @line = split (" ",$_);
		$block_num = $line[2];
		
		if ( ($line[10] >= $maxMEC) || ($line[6] <= $min_phased)){
			#print "$line[6]\n";
			$skip_block=1;
			$discarded_blocks_2++;
			next;
		}
		
		else {
			#print "unblocked\n";
			$phased_var_2 += $line[6];
			$skip_block=0;
			$good_blocks_2++;
			next;
		} 
		
	}
	
	elsif ($_ =~ m/^\*/ ) {
		#$changing=1; 
		next;
	}
	
	else {
		my @line = split ("\t", $_);
		if ($skip_block != 1) {
			#print $hashes_ref[$i];
			my $coordinate = $line[3]."-".$line[4];
			@{$P2_phase{$coordinate}} = ($line[1], $block_num);
			
		}
		
		else {
			next;
		}
		
	}

}
close (HAPCUT2);

#print Dumper(\%P1_phase);
#print Dumper(\%P2_phase);


if ($parents =~ /.*\.gz/) {
	print "Reading parents as .gz file\n";
	#exit();
	open (PARENTS_VCF, "gunzip -c $parents |");
}
else{
	#exit();
	open (PARENTS_VCF, "<$parents");
}



my $PL_pos;
while (<PARENTS_VCF>) {

	if ($_ =~ /^##/){
		next;
	}

	elsif ($_ =~ m/^#CHROM/) {
		chomp;
		my @P_names= split (/\t/, $_);
		#print scalar(@P_names);	
		print STDERR "Reading bi-parental file $parents\n";
		die "Parental file is not in a correct format, make sure only two sample are genotyped in PL format (bcftools)\n" if (scalar(@P_names) != 11);
		my $parent1_string = ($P_names[9]);
		$parent1 = (split ("\/", $parent1_string))[-1];
		my $parent2_string = ($P_names[10]);
		$parent2 = (split ("\/", $parent2_string))[-1];
		next;
	}
	
	else{
        
		chomp;
		my @line = split (/\t/, $_);
        
        if (!defined $PL_pos) {
            $PL_pos = gather_format(@line);
        }
		
        my @parent1_calls = split (/,/, (split(":",$line[9]))[$PL_pos]);
		my @parent2_calls = split (/,/, (split(":",$line[10]))[$PL_pos]);
		my @op=();
		my $coordinate =  $line[0]."-".$line[1];
		#print $coordinate."\n";
		#my $block_num;
		
			#TESTCROSS for parent 1
			if 	($parent1_calls[1] == 0 && $parent1_calls[0] > $lkh && $parent1_calls[2] > $lkh && $parent2_calls[1] > $lkh) {      
				
				#print "Good testcross P1\n";
				
				#print $coordinate."\n";
				if (exists $P1_phase{$coordinate}) {
					#print "Found in P1 phases\n";
					$block_num = $P1_phase{$line[0]."-".$line[1]}[1];
					if ($P1_phase{$line[0]."-".$line[1]}[0] == 1) {
						@op = ("TESTC1","ph1", $block_num); 
						my $TC_type = calc_TC_type ($parent2_calls[0], $parent2_calls[2], $op[1]);
						push (@op, $TC_type);
						$count_phased_P1++;
						}
					else{ 
						@op = ("TESTC1","ph0",$block_num);
						my $TC_type = calc_TC_type ($parent2_calls[0], $parent2_calls[2], $op[1]);
						push (@op, $TC_type);
						$count_phased_P1_f++; 
						}
				}
				
				else {
					#print "Unphased\n";
					$count_unphased_P1++;
				}	
				
			}		
			
			#TESTCROSS for parent 2
			elsif ($parent2_calls[1] == 0 && $parent2_calls[0] > $lkh && $parent2_calls[2] > $lkh && $parent1_calls[1] > $lkh) {
				
				#print "Good testcross P2\n";
				
				if (exists $P2_phase{$coordinate}) {
					#print "Found in P2 phases\n";
					$block_num = $P2_phase{$line[0]."-".$line[1]}[1];
					if ($P2_phase{$line[0]."-".$line[1]}[0] == 1) {
						
						@op = ("TESTC2","ph1", $block_num);
						my $TC_type = calc_TC_type ($parent1_calls[0], $parent1_calls[2], $op[1]);
						push (@op, $TC_type);
						$count_phased_P2++;
						}
					else { 
						@op = ("TESTC2","ph0", $block_num);
						my $TC_type = calc_TC_type ($parent1_calls[0], $parent1_calls[2], $op[1]);
						push (@op, $TC_type);
						$count_phased_P2_f++; 
						}
				}
				
				else {
					#print "Unphased\n";
					$count_unphased_P2++;
				}	
			
			
			}		
			
			else {
				#print "Not testcross\n";
				$not_TC++;
			}
		## append to op array which ref/alt nucletotide is representing the homozygous call for the other parent
			
		
			if (defined $op[3] && (length $op[3]) > 1) {
				@{$archive{$line[0]}{$line[1]}} = @op;#
			}
			
	}

}


close (PARENTS_VCF);

undef $PL_pos;

%P1_phase =();
%P2_phase =();

#print Dumper (\%archive);

open (TESTC1, ">$hap1\.testcross.vcf");
open (TESTC2, ">$hap2\.testcross.vcf");

####### Fetch progeny numlines and individuals 


my $num_lines;
if ($progeny =~ /.*\.gz/) {
	$num_lines = (split (" ",`zcat $progeny | wc -l `))[0];
	open (PROG, "gunzip -c $progeny |");
	print "Reading progeny as .gz file\n";
	}
else {
	$num_lines = (split (" ",`wc -l $progeny`))[0];
	open (PROG, "<$progeny");
}

while (<PROG>) {
	if ($_ =~ m/^#CHROM/) {
			my @CHROM_line = split(/\t/, $_);
			$nind= scalar(@CHROM_line) - 9;
			print STDERR "Reading progeny file $progeny with $nind individuals and $num_lines lines\n";
			last;
		}

}
close (PROG);
#################################################

my $progress = Term::ProgressBar->new ({count => $num_lines, fh => \*STDERR});
my $line_progress = 1;


if ($progeny =~ /.*\.gz/) {
	#exit();
	open (PROG, "gunzip -c $progeny |");
}
else{
	open (PROG, "<$progeny");
}


my $buffer;
my $buffer_TC1;
my $buffer_TC2;
my $last_scaff;

while (<PROG>) {
	
	$progress->update ($line_progress++);
    
	if ($_ =~ m/^#/ ) {
		print TESTC1 $_;
		print TESTC2 $_;
		next;
		}
	else {
		
		chomp;
		my @lineP = split(/\t/, $_);		 # now line is internal to this loop, refering to the values in the progeny file
		if (!defined $PL_pos) {
            $PL_pos = gather_format(@lineP);
        }
        $count_snps++;
		#print "$lineP[0]\t$lineP[1]\n";
		if ((defined $last_scaff) && (($lineP[0] ne $last_scaff) || eof)){
			print TESTC1 $buffer_TC1;
			print TESTC2 $buffer_TC2;
			$buffer_TC1='';
			$buffer_TC2='';
		}
		$last_scaff = $lineP[0];
		
		
		
		if (exists $archive{$lineP[0]}{$lineP[1]}) {
			

			
			if ($archive{$lineP[0]}{$lineP[1]}[1] eq "ph1") {
				@lineP=flip(@lineP);
			}
			
			## set line to print, after flipping eventually
			my @lineP_printable = @lineP;
			$lineP_printable[0]=$lineP_printable[0]."_BLOCK_".$archive{$lineP[0]}{$lineP[1]}[2]."_".$archive{$lineP[0]}{$lineP[1]}[3];
			my $print_out= join("\t", @lineP_printable)."\n";
	
			
############# set filehandles and buffer for correct printing of current line ######################

			if    ($archive{$lineP[0]}{$lineP[1]}[0] eq "TESTC1") {
				$filehandle = *TESTC1; # used for direct printing of type1
				$buffer = \$buffer_TC1; #used to store buffer for type2
				$count_snps_1++;
			}
			
			elsif ($archive{$lineP[0]}{$lineP[1]}[0] eq "TESTC2") {
				$filehandle = *TESTC2;
				$buffer = \$buffer_TC2;
				$count_snps_2++;
			}
			
			else {
				die "Some inconsistency in the TC attribution\n";
			}
			
#####################################################################################################			
			##type1 directly printed
			if ($archive{$lineP[0]}{$lineP[1]}[3] eq "TC-type1") {
				print $filehandle "$print_out";
				next;	
			} 
			##type2 stored temporarely in the buffer
			elsif ($archive{$lineP[0]}{$lineP[1]}[3] eq "TC-type2") {
				${$buffer}.= $print_out;
				next;
			
			}
			
			else {
				die "Incosistency with TC type, please report bug to davide.scaglione.agct\@gmail.com\n";
			}
		}	
######################################################################################################			
			
		
			
		## case where position is not present in the archive at all
		else {
			$progeny_discarded_SNPs++;
			next;
			
		}	
		
	$last_scaff = $lineP[0];	
    }
}

close(PROG);
close(TESTC1);
close(TESTC2);	
##
sub gather_format {
    my @line = @_;
    my @FORMAT = split (":", $line[8]);
    my @index = grep { $FORMAT[$_] =~ /PL/ } 0..$#FORMAT;
    #print STDERR "PL position is parsed at $index[0]\-index FORMAT field\n";
    return $index[0];
}

sub flip {
    my @int_line= @_;
	
    my $PL_pos = gather_format(@int_line);
    
    #REVERT ALLELES
	($int_line[3], $int_line[4]) = ($int_line[4], $int_line[3]);     #input to the funct is an array with the fields of the line
		
	#complement to 1 of AF1
	if ($int_line[7] =~ /AF1?=(0*\.*[0-9]*)/) {
		my $newAF1 = 1-$1;
	    $int_line[7] =~ s/(AF1?=)0*\.*[0-9]*/$1$newAF1/;
	}
	
	
	# flip all genot likelihood
	for (my $i = 0; $i < $nind ; ++$i) {
			my @individual = split (":", $int_line[$i+9]);
            my @genot = split (",", $individual[$PL_pos]);
			($genot[0],$genot[2]) = ($genot[2],$genot[0]);
            $individual[$PL_pos] = join(",",@genot);           
			$int_line[$i+9] = join (":",@individual);
	}
	
	return @int_line;
}
	
#
sub calc_TC_type {
	my ($homo_alt, $homo_ref, $phase) = @_;
	
	if	  ($phase eq "ph0" && $homo_alt == 0) {
		return "TC-type1";
	}
	elsif ($phase eq "ph1" && $homo_ref == 0) {
		return "TC-type1";
	}
	elsif ($phase eq "ph0" && $homo_ref == 0) {
		return "TC-type2";
	}
	elsif ($phase eq "ph1" && $homo_alt == 0) {
		return "TC-type2";
	}
}	
	
	
print "

Number of individuals in progeny:\t$nind

*Not testcross SNPs from parents:\t$not_TC
*Total number of SNPs in progeny file:\t$count_snps

\=== PARENT $parent1 ==== (MEC <= $maxMEC and phased SNPs => $min_phased)

HAPCUT (information from headers)
Discarded blocks :\t\t$discarded_blocks_1
Good blocks:\t\t\t$good_blocks_1
Total good phased SNPs:\t\t$phased_var_1

HAPCUT+PARENTS
Phase 0 testcross SNPs:\t\t$count_phased_P1
Phase 1 testcross SNPs:\t\t$count_phased_P1_f
Unphased testcross SNPs:\t$count_unphased_P1\t(scored as testcross but either not phased or discarded block)

PROGENY
Phased SNPs from progeny:\t$count_snps_1


\=== PARENT $parent2 ==== (MEC <= $maxMEC and phased SNPs => $min_phased)

HAPCUT (information from headers)
Discarded blocks :\t\t$discarded_blocks_2
Good blocks:\t\t\t$good_blocks_2
Total good phased SNPs:\t\t$phased_var_2

HAPCUT+PARENTS
Phase 0 testcross SNPs:\t\t$count_phased_P2
Phase 1 testcross SNPs:\t\t$count_phased_P2_f
Unphased testcross SNPs:\t$count_unphased_P2\t(scored as testcross but either not phased or discarded block)

PROGENY
Phased SNPs from progeny:\t$count_snps_2

\===========================================
Progeny discarded SNPs:\t\t$progeny_discarded_SNPs
";


=head1 NAME

TC_separator.pl

=head1 SYNOPSIS

Use:

perl TC_separator.pl    --HAP1 <parent1_hapcut_output> --HAP2 <parent2_hapcut_output> --parents <parents.vcf> --progeny <progeny.vcf>

perl TC_separator.pl    --help      

Statistics will be printed in STDOUT

=head1 DESCRIPTION

This script takes as inputs two indipendendly phased references by HAPCUT software, 
a vcf file containing bi-parental variant calling, a vcf with progeny-wide variant calling. 
It generates two test-cross phase-sorted vcf files to be used with next steps of this pipelines 
(i.e. gt-hmm.pl).
Please note that when a REF<=>ALT flip is required for a given position, the output VCF file will 
have flipped REF, ALT columns - AF(1) field as complement to 1 - flipped likelihood values.
Any other field of the VCF remains unchanged. 

=head1 ARGUMENTS

TC_separator.pl takes the following arguments:

=over 4

=item help
  --help

  Displays the usage message.


=item HAPCUT parent 1

  --HAP1 hapcut_output_file

(Required.) This is the phase information required for the first individual in the bi-parental variant calling file.

=item HAPCUT parent 2

  --HAP2 hapcut_output_file

(Required.) This is the phase information required for the second individual in the bi-parental variant calling file.

=item parents VCF

  --parents parents.vcf

(Required.) bi-parental vcf file (Must contain the PL INFO field).

=item progeny VCF

  --progeny progeny.vcf

(Required.) progeny-wide vcf file (Must contain the PL INFO field).

=item maximum allowed MEC score

  --max-MEC INT (Default: 500)

(Optional.) Maximum MEC score to accept a phased block from HAPCUT. Can be set to higher values when dealing with very long phase blocks on average.

=item minimum phased SNPs

  --min-phased INT  (Default: 10)

(Optional.) Minimum number of phased SNPs to accept a phased block. 

=item maximum genotype likelihood

  --LKH INT (Default: 25)

(Optional.) Minimum value for alternative genotypes negative logarithm likelihood to accept calls from bi-parental VCF file (Default: 25)
It might requires tuning if using calling algorithms other than samtools mpileup. Increse this value to make analysis more
stringent only considering very robust calls. 


=head1 AUTHOR

Davide Scaglione, E<lt>davide.scaglione.agct@gmail.comE<gt>.

=head1 COPYRIGHT

This program is distributed under GNU GPL.

=head1 DATE

6-Jun-2013

=cut





