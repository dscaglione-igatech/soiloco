#!/usr/bin/perl
#####################################
#            gt-gmm.pl              #
#                                   #
#        Davide Scaglione           #
#                                   #
#####################################
use strict;
use warnings;
use Getopt::Long;
use vars qw($opt_i $opt_o $opt_c $opt_e $opt_f $opt_H $opt_b $opt_S $opt_L $opt_l $opt_d $help);   
use Pod::Usage;
GetOptions('STRINGS=s'=>\$opt_i, 'OUTPUT=s'=>\$opt_o, 'switch-prob=f'=>\$opt_c, 'het-err=f'=>\$opt_e, 'hom-err=f'=>\$opt_f, 'HCALL-prob=f'=>\$opt_H, 'HCALL-tweak=f'=>\$opt_b, 'min-string=i'=>\$opt_S, 'min-LOD=f'=>\$opt_L, 'length-ratio=f'=>\$opt_l, 'density-ratio=f'=>\$opt_d, 'help'=>\$help);


my $SWITCH          = 10E-8;  			#6 is the minimum I can use, 7 looks better to mantain het call in AAAAA or BBBBB states to the end
my $HET_ERROR		= 0.01;				# error for single-SNP false heterozygous call
my $HOM_ERROR		= 0.01;				# error for single -SNP false alternative allele call
my $HET_CALL		= 0.2;				# probability for HET calls over homozygous call in het scaffold (models the coverage)
my $bonus			= 1;				# this value different from 1 make this to become a conditional random fields rather than HMM..workaround to give more weight to het calls
my $min_string		= 5;				# min length of a string to start any estimation
my $mylod			= 2;				#minumum lod difference between best and second path calls
my $length_ratio	= 0.2;				#max length ratio, below this extra calls are not considered
my $density_ratio	= 0.2;				#max density ratio, below this extra calls are not considered

$SWITCH		 	= $opt_c if $opt_c;
$HET_ERROR		= $opt_e if $opt_e;
$HOM_ERROR		= $opt_f if $opt_f;
$HET_CALL		= $opt_H if $opt_H;
$bonus			= $opt_b if $opt_b;				
$min_string		= $opt_S if $opt_S;				
$mylod			= $opt_L if $opt_L;					
$length_ratio  = $opt_l if $opt_l;				
$density_ratio = $opt_d if $opt_d;

Pod::Usage::pod2usage( -verbose => 2 ) if ($help  || !$opt_i || !$opt_o);


my $pos;

# Transition Matrix###################################################################################


my $self = 1-$SWITCH;
my $switch = (1 - $self) / 2;                                   #Define $switch on the basis of self
my @TM = (
	[$self,   $switch, $switch], # 0 = A						#array of array: $i = starting state (0,1,2), $j= switching state 
	[$switch, $self,   $switch], # 1 = B
	[$switch, $switch, $self],   # 2 = H
);

print "Transition Matrix\n";
for (my $i = 0; $i < @TM; $i++) {
	for (my $j = 0; $j < @{$TM[$i]}; $j++) {      				#need to reference array on the array
		print "$i $j $TM[$i][$j]\n";
	}
}

#################### Emission Matrix#########################################	#initiate emission prob matrix
my @EM = (
	[1 - $HET_ERROR - $HOM_ERROR, $HOM_ERROR, $HET_ERROR],		
	[$HOM_ERROR, 1 - $HOM_ERROR - $HET_ERROR, $HET_ERROR],
	#[0.5 - $HOM_ERROR, 0.5 - $HOM_ERROR,  $HET_CALL - $HET_ERROR]	
	[(1 - $HET_CALL)/2, (1 - $HET_CALL)/2, $HET_CALL],	#HET_CALL prob of het emission, to modulate on the basis of coverage
);
print"Emission Matrix\n";
for (my $i = 0; $i < @EM; $i++) {
	for (my $j = 0; $j < @{$EM[$i]}; $j++) {
		print "$i $j $EM[$i][$j]\n";
	}
}
############################################################################################

open (IN, "$opt_i");
open (OUT, ">$opt_o");

############################################################################################
# main loop #
#############
while (<IN>) {
	if ($_ =~ /^\s*$/) {next;}
	
	chomp;
	my ($contig, $sample, $seq, $pos) = split ("\t", $_);#
	my @Name = split ("/", $sample);    
		$sample = $Name[-1];
	
	
	print OUT "$contig\t$sample\t";
	if (!defined $seq) {print OUT "CASE=No;0;0\n";next;}
	
	
	my $length = length $seq;
	
	if ( $length < $min_string) {print OUT "Sh;NA;$length\n"; next;}
##############################################	
#											 #
	my ($PROBS, $PATHS) = decode($seq);            # call the VITERBI function (CRF) to generate two best paths in @PATHS and @PROBS
#											 #	
##############################################	
	#print "$contig\t$sample\t$$PROBS[0]\t$$PATHS[0]\t$$PROBS[1]\t$$PATHS[1]\n";
	
	
	
	#my @pos= split (",",$pos);
	#for (my $i=0; $i < @pos; $i++) { print "$pos[$i]\n";}
	
	my $LOD = $$PROBS[1]-$$PROBS[0];
	
	
	
	
	# need to dereference the array
	my @AoH1 = partition ($$PATHS[0], $pos);  # partitioning best path --> return start,end,call, SNP density, SNP count
	my @AoH2 = partition ($$PATHS[1], $pos);  # partitioning second best path
	
	
	#### BELOW, several differen cases and behaviors of this script####
	
	## CASE 1...two continuos paths competing...@AoH1 == 1 && @AoH2 == 1, I will use LOD score to take a decision....
	
	if (@AoH1 == 1 && @AoH2 == 1 && ( $LOD > $mylod)) {
																	# in this case just need to report the best path (this case is where we really distinguish full-hom vs full-het paths
		
		printf OUT "1A;%.3f;%i\t", $LOD, $length;
		print OUT "$AoH1[0]{start}".":"."$AoH1[0]{call}".":"."$AoH1[0]{end}\n";

		
	}
	
	## CASE 2..second path is more than 1..probably most of the cases for false switching a the end
	elsif (@AoH1 == 1 && @AoH2 > 1) {
	
		if ($LOD > $mylod){    # CASE A - if LOD still is higher than threshold...just keep the first path as good

			
			printf OUT "2A;%.3f;%i\t", $LOD, $length;
			print OUT "$AoH1[0]{start}".":"."$AoH1[0]{call}".":"."$AoH1[0]{end}\n";
		
		}
		
		else {
			#print "CASE2B!!\n"; # here need to check if a lenght-clip of second path can solve...only want to clip sides, no merging of non continuos calls..check if just one path remains afterwards and if the call match the first path one; this is helpful for rescuing short scaffolds which have little score changes
		
			my @AoH2_length_clipped = cliplength (@AoH2);
			my @AoH2_density_clipped = clipdensity (@AoH2);
			
			#print "TEST CLIPPED:".scalar(@length_clipped_2);
			
			if (@AoH2_length_clipped == 1 && ($AoH2_length_clipped[0]{call} eq $AoH1[0]{call})) {
		
				
				printf OUT "2L;%.3f;%i\t", $LOD, $length;
				print OUT "$AoH1[0]{start}".":"."$AoH1[0]{call}".":"."$AoH1[0]{end}\n";
			}
			
			
			elsif (@AoH2_density_clipped == 1 && ($AoH2_density_clipped[0]{call} eq $AoH1[0]{call})) {
		
				
				printf OUT "2D;%.3f;%i\t", $LOD, $length;
				print OUT "$AoH1[0]{start}".":"."$AoH1[0]{call}".":"."$AoH1[0]{end}\n";
			
			}
			
			else {
			
			printf OUT "N2;%.3f;%i\n", $LOD, $length;
			
			}
		
		
		}
	}
	
	
	
	
	
	# CASE 3..first best paths is more than 1..VERY CAREFUL! It can be...errors on the edges, in the middle or crossingover...test with length and density##
	elsif (@AoH1 > 1) {
		#print "CASE3!!\n";
		# if one path significantly dominate by density or length take it, otherwise mark it as a possible recombination event
		# print "original path1\n";
		# print "$AoH1[0]{start}".":"."$AoH1[0]{call}".":"."$AoH1[0]{end}\n";
		# print "$AoH1[1]{start}".":"."$AoH1[1]{call}".":"."$AoH1[1]{end}\n";
		# print "original path2\n";
		# print "$AoH2[0]{start}".":"."$AoH2[0]{call}".":"."$AoH2[0]{end}\n";
		# print "$AoH2[1]{start}".":"."$AoH2[1]{call}".":"."$AoH2[1]{end}\n";
		# print "$AoH2[2]{start}".":"."$AoH2[2]{call}".":"."$AoH2[2]{end}\n";
		
		my @AoH1_lenght_clipped = cliplength (@AoH1);
		my @AoH1_density_clipped = clipdensity (@AoH1);
		

		
		if (@AoH1_density_clipped == 1) {
			
			
			printf OUT"3D;%.3f;%i\t", $LOD, $length;
			print OUT "$AoH1_density_clipped[0]{start}".":"."$AoH1_density_clipped[0]{call}".":"."$AoH1_density_clipped[0]{end}\n";
		}
		
		elsif (@AoH1_lenght_clipped == 1) {
			
			
			printf OUT "3L;%.3f;%i\t", $LOD, $length;
			print OUT "$AoH1_lenght_clipped[0]{start}".":"."$AoH1_lenght_clipped[0]{call}".":"."$AoH1_lenght_clipped[0]{end}\n";
		} 
		
		else {				# AoH1 is still > 1 after clipping -> try to asses if can be a recombination/misassembly..both paths need to corroborate but for a last switch
			
			
			
			my @AoH2_length_clipped = cliplength (@AoH2);   # clip by lenght the second path
			
			#print "$AoH2_length_clipped[0]{start}".":"."$AoH2_length_clipped[0]{call}".":"."$AoH2_length_clipped[0]{end}\t"."$AoH2_length_clipped[1]{start}".":"."$AoH2_length_clipped[1]{call}".":"."$AoH2_length_clipped[1]{end}\n" ;
			
			
			
			
			
			my @AoH1_sorted =  sort { $b->{length} <=> $a->{length} } @AoH1; # need to sort this guy since AoH2 was returned as sorted array
			@AoH1_sorted =  sort { $a->{start} <=> $b->{start} } @AoH1_sorted;
			
			#print scalar(@AoH1_sorted)."\n";
			
			# print $AoH1_sorted[0]{call};
			# print $AoH2_length_clipped[0]{call};
			
			
			
			if ( (@AoH2_length_clipped == 2) && (@AoH1_sorted == 2) &&  ($AoH1_sorted[0]{call} == $AoH2_length_clipped[0]{call}) && ($AoH1_sorted[1]{call} == $AoH2_length_clipped[1]{call}) ) {
			
				printf OUT "3R;%.3f;%i\t", $LOD, $length;
				print OUT "$AoH1_sorted[0]{start}".":"."$AoH1_sorted[0]{call}".":"."$AoH1_sorted[0]{end}\t"."$AoH1_sorted[1]{start}".":"."$AoH1_sorted[1]{call}".":"."$AoH1_sorted[1]{end}\n" ;
			}
			else {
			
				printf OUT "N3;%.3f;%i\n", $LOD, $length;
			
			}
		
		}
	
	
	
	
	
	
		# for (my $a = 0; $a < @AoH1; $a++){
		# print $AoH1[$a]{start}.":".$AoH1[$a]{call}.":".$AoH1[$a]{end}." ".$AoH1[$a]{density}."\n";
		# }
	
	
	
	# my @clipped1 = clipdensity (@AoH1);
	# print "AFTER CLIPPING\n";
		# for (my $a = 0; $a < @clipped; $a++){
		# print $clipped[$a]{start}.":".$clipped[$a]{call}.":".$clipped[$a]{end}." ".$clipped[$a]{density}."\t";
		# }##
		
	
		
		
	}
	else {
	printf OUT "NN;%.3f;%i\n", $LOD, $length;
	}
}

close (IN);
close (OUT);
###################################################################################################################
sub decode {
	my ($seq) = @_;
	
	# DP Matrix
	my @DP; # 3D matrix of [pos][state]{score/trace}
	
	# initialization - revisit this later     					# need to initialize with starting probs since 
	my $s = substr($seq, 0, 1);									# we do not have any previous prob to multiply for ...here his log trasf
	$s =~ tr/ABH/012/;											#transliterate ABH to 012 to make easier for looping arrays
	$DP[0][0]{score} = -log(1/4 * $EM[0][$s])/log(10);						#we can chane this to different starting probs based on the population
	$DP[0][1]{score} = -log(1/4 * $EM[1][$s])/log(10);						#NOW SET FOR A F2 pop
	$DP[0][2]{score} = -log(1/2 * $EM[2][$s])/log(10);
	$DP[0][0]{trace} = 0;
	$DP[0][1]{trace} = 1;
	$DP[0][2]{trace} = 2;
	
	# fill
		for (my $i = 1; $i < length($seq); $i++) { 		    # for every position but the very first one which we initialized previously
		my $s = substr($seq, $i, 1);
		$s =~ tr/ABH/012/;
		for (my $j = 0; $j < @TM; $j++) { 			    # for each state (looking at the transition matrix)
			my $emit = $EM[$j][$s];					    # lookup the emission: $j is the state we are looking and $s the actual emission
			die "emit unknown for $s\n" if not defined $emit;
			my $max_prob = 1000;       				    # initialize to -1 to allow first cycle to be true and be stored
			my $max_prev_state;
			for (my $k = 0; $k < @TM; $k++) { 		    # for each previous state
				my $trans = $TM[$k][$j];			    # retrieve trans prob given $k is the previous state and $j is the current state
				my $prev = ($DP[$i-1][$k]{score});	    # get previous score $i-1 observation $k state  # log transformed here
				#my $prob = $emit * $trans * $prev;	    # calculate the prob for this transition
				my $prob = (-log($emit)/log(10)) + (-log($trans)/log(10)) + $prev;   # log tranformed here
                #print "$emit $trans $prev $prob\n";# die;
				if ($prob < $max_prob) {
					$max_prob = $prob;				    # if max prob --> store it
					$max_prev_state = $k;			    # and store its previous state trace back
				}
			}										# exit the loop of previous states
			$DP[$i][$j]{score} = $max_prob;         # populate the DP matrix for the current state with the best values
			$DP[$i][$j]{trace} = $max_prev_state;	# and related trace back
		}
	}
	
	my %rank=();
	for (my $i = 0; $i < @TM; $i++) {				# for each state
		$rank{$i} = $DP[@DP-1][$i]{score};			# store the last score in a little hash form the DP matrix
	}

	
	my @paths =();								# clean the PATHS rank array
	my @probs=();								# clean the PROBS rank array
		#print "\n";
	
	foreach my $last_state (sort {$rank{$a} <=> $rank{$b}} keys %rank) {   # had to swith since with log now the lower is better
		#print "$last_state\t";
		
		#my $path = "";									# BUG????? Actually I'm missin the last state
		
		my $path = $last_state;							# start the path (backward) with the last (max score) state
		my $i = @DP - 1;								# starting from the last position
		my $state = $last_state;							# consider the best score state
		
		while ($i > 0) {								# till the second, which will look at the first one
			my $prev = $DP[$i-1][$state]{trace};		# get the trace back for the previous state
			$path .= $prev;								# append to the path
			$state = $prev;								# update the state for the next back step
			$i--;
		}
		$path = reverse $path;							# reverse the path (since we were appending backward
		
		##print "$path\n";
		#print "$rank{$last_state}\n";
		my $logscale = $rank{$last_state}; # here its already in log scale
		#print "$logscale\n";
		
		push (@paths, $path);
		push (@probs, $logscale);
	}
	
#print join("\t", @probs);
return (\@probs, \@paths);  # reference the arrays to be collected by as scalar in the array of ($PROBS, $PATHS)
#print "\n"; 	
}
######################################################################################################


sub partition {

	my $string = $_[0];
	my $pos = $_[1];
	my @AoH;
	my @POS= split (",",$pos);
	my $prev = substr ($string, 0, 1);	# initialize first call
	$AoH[0]{start} = $POS[0];           #initialize first start
	#print "$prev\n";
	my $p = 0;
	$AoH[$p]{count}=1;				# put in count the first call
	
	#print join(" ", @POS)."\n"; 
	
	for (my $a = 1; $a < @POS; $a++) {
		my $site = substr($string,$a,1);
		#print "$a\t"; 
		if ( $site == $prev) {
			#print "$site\t$prev\t$POS[$a]\n";
			#print "something happens\n";
			$AoH[$p]{count}++;
			$prev = $site;
		}
		
		else  {
			$AoH[$p]{call}=$prev;
			$AoH[$p]{end}=$POS[$a-1];
			$AoH[$p]{length} = $AoH[$p]{end} - $AoH[$p]{start} + 1;
			#print "Totalizzata lunghezza di $AoH[$p]{length}\n";
			$AoH[$p]{density} = ($AoH[$p]{count}) / $AoH[$p]{length};
			
			$p++;
			
			$AoH[$p]{start} = $POS[$a];
			#print "Questo nuovo start � $AoH[$p]{start}\n";
			$AoH[$p]{count}++;
			$prev = $site;
			#print "fine!!\n" 
		}
		
		
		
		#print 	"$AoH[$p]{count}....\n";
	}

	
	$AoH[$p]{call} = substr ($string, -1, 1);     #close last partition
	$AoH[$p]{end} = $POS[-1];						#close last partition
	$AoH[$p]{length} = $AoH[$p]{end} - $AoH[$p]{start} + 1;
	#print "Totalizzata lunghezza di $AoH[$p]{length}\n";
	$AoH[$p]{density} = $AoH[$p]{count} / $AoH[$p]{length};
	
	# print "Ultima call $AoH[$p]{call}\n";
	# print "Ultima posizione $AoH[$p]{end}\n";
	# print "Ultima lunghezza $AoH[$p]{length}\n";
	return (@AoH);
	
}

#########################################################################

sub clipdensity {

	my @sorted =  sort { $b->{density} <=> $a->{density} } @_;
	#print "\nUnclipped_sorted\n";
	#print join "\t", map {" End: ".$_->{end}." - Density: ".$_->{density}} @sorted;  
	
	push (my @clipped, $sorted[0]);  
	
	for (my $i=1; $i < @sorted; $i++){
		
		
		if  ($sorted[$i]{density}/$sorted[0]{density} > $density_ratio ){ 			# set here the max density ratio with respect to the highest
			push (@clipped, $sorted[$i]);								#if density is higher than ratio, the paths is kepts and appended
			#print "something";
		}
		
		
	}
	#print "\nClipped\n";
	#print join "\t", map {$_->{start}."-".$_->{end}." with density ".$_->{density}} @clipped;
	
	@clipped =  sort { $a->{start} <=> $b->{start} } @clipped;   # a sorting by start when double path remain for putative misassembly or recomb

	return (@clipped);

}

################################################################################################################

sub cliplength {					# this is just ment to be used for second paths which might have some artifical switch at the end

	my @sorted =  sort { $b->{length} <=> $a->{length} } @_;
	#print "\nUnclipped_sorted\n";
	#print join "\t", map {$_->{start}.":".$_->{call}.":".$_->{end}."\t" } @sorted;  
	
	push (my @clipped, $sorted[0]);  
	
	for (my $i=1; $i < @sorted; $i++){
		
		
		if  ($sorted[$i]{length}/$sorted[0]{length} > $length_ratio ){ 			# put here the max length ratio with respect to the highest
			push (@clipped, $sorted[$i]);								# if this does not happend only the longest remain in the array
			#print "something";
		}
		
		
	}
	#print "\nClipped\n";
	#print join "\t", map {$_->{start}.":".$_->{call}.":".$_->{end}."\t" } @clipped;

	@clipped =  sort { $a->{start} <=> $b->{start} } @clipped;  # a sorting by start when double path remain for putative misassembly or recomb
	
	return (@clipped);

}

=head1 NAME

gt-hmm.pl

=head1 SYNOPSIS

Use:

perl gt-hmm.pl   --STRINGS=<vcf2strings output> --OUTPUT=<outputh path> [OPTIONS]

perl gt-hmm.pl    --help      

Statistics will be printed in STDOUT

=head1 DESCRIPTION

This script takes as input vcf2strings output and perform the core HMM-based genotype imputation.
The output file will contain three columns: 1)scaffold 2)individual 3)string report flags 4)call boundaries.


=head1 ARGUMENTS

gt-hmm.pl takes the following arguments:

=over 4

=item help
  --help

  Displays the usage message.


=item STRINGS input file

  --STIRNGS <vcf2strings_output>

(Required.) Provide string-converted variant calls per individual.

=item OUTPUT

  --OUTPUT <output_path>

(Required.)

=head1 OPTIONS

=item Switch probability

  --switch-prob float	(Default: 10E-8)

Prabability of trasition from a genotype state to another.

=item Heterozygous error probability

  --het-err float	(Default: 0.01)

Probability of false heterozygous emission.

=item Homozygus error probability

  --hom-err float (Default: 0.01)

Probability of false alternative homozygous emission.

=item Heterozygous call probability 

  --HCALL-prob float	(Default: 0.2)

Probability of heterozygous sites to be called as hets by the presence of reads representing both alleles. 
IMPORTANT: This parameter must be carefully adjusted on the basis of the average coverage. 0.2 showed good performance with coverage ~1X.

=item Minimum string length

  --min-string INT	(Default: 5)

Minimum length of a string (i.e run of called sites) to be processed for imputation.

=item Minimum LOD

  --min-LOD INT	(Default: 2)

Minimum LOD to accept an imputation.
LOD is defined as the negative logarithm ratio of probabilities between the best decoded path and the second one by using Viterbi algorithm.


=item Length ratio

  --length-ratio float	(Default: 0.2)

Minimum length ratio to allow path clipping.
When two alternative path are decoded from the same scaffold/block, clipping of the shortest path is allowed when minimum length ratio is satisfied. 

=item Density ratio

  --density-ratio float	(Default: 0.2)

Minimum density ratio to allow path clipping.
When two alternative path are decoded from the same scaffold/block a clipping routine will try remove suspicious SNP-rich regions calling the secondary path. This system is ment to overcome false path switching by run of erroneous calls caused by repetitive regions. Clipping is allowed when the ratio of SNP densities between two path is satisfied.  


=head1 AUTHOR

Davide Scaglione, E<lt>davide.scaglione.agct@gmail.comE<gt>.

=head1 COPYRIGHT

This program is distributed under GNU GPL.

=head1 DATE

6-Jun-2013

=cut

